-- Basic Playdate test - draw image to screen
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

-- DITHER TYPES: 
-- From https://sdk.play.date/1.12.0/Inside%20Playdate.html#m-graphics.image.blurredImage
-- playdate.graphics.image.kDitherTypeNone
-- playdate.graphics.image.kDitherTypeDiagonalLine
-- playdate.graphics.image.kDitherTypeVerticalLine
-- playdate.graphics.image.kDitherTypeHorizontalLine
-- playdate.graphics.image.kDitherTypeScreen
-- playdate.graphics.image.kDitherTypeBayer2x2
-- playdate.graphics.image.kDitherTypeBayer4x4
-- playdate.graphics.image.kDitherTypeBayer8x8
-- playdate.graphics.image.kDitherTypeFloydSteinberg
-- playdate.graphics.image.kDitherTypeBurkes
-- playdate.graphics.image.kDitherTypeAtkinson


import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'

print( "Example ~ Image effects ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

logo = gfx.image.new( "images/cat.png" )

function playdate.update()
  gfx.clear( gfx.kColorWhite )
  
  crankAngleDegrees = playdate.getCrankPosition()
  
  logo:draw( 0, 0 )
  logo:drawBlurred( 50, 0,    crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeDiagonalLine )
  logo:drawBlurred( 100, 0,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeVerticalLine )
  logo:drawBlurred( 150, 0,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeHorizontalLine )
  logo:drawBlurred( 200, 0,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeScreen )
  logo:drawBlurred( 250, 0,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeBayer2x2 )
  logo:drawBlurred( 300, 0,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeBayer4x4 )
  logo:drawBlurred( 350, 0,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeBayer8x8 )
  logo:drawBlurred( 50, 50,   crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeFloydSteinberg )
  logo:drawBlurred( 100, 50,  crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeBurkes )
  logo:drawBlurred( 150, 50,  crankAngleDegrees, 1, playdate.graphics.image.kDitherTypeAtkinson )
    
end
