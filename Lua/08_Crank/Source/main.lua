-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'

print( "Example ~ Crank test ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

function playdate.update()
  gfx.clear( gfx.kColorWhite )

  -- Playdate 0 degrees = up,
  -- whereas with normal trig, 0 degrees = to the right on the unit circle
  -- thus the offset of -90:
  crankAngleDegrees = playdate.getCrankPosition() - 90
  crankAngleRadians = math.rad( crankAngleDegrees )
  crankChangeDegrees = playdate.getCrankChange()

  cosine = math.cos( crankAngleRadians )
  sine = math.sin( crankAngleRadians )

  radius = 50

  x1 = 400/2
  y1 = 240/2
  x2 = x1 + (cosine * radius)
  y2 = y1 + (sine * radius)

  gfx.drawText( "Crank position: " .. tostring( crankAngleDegrees ), 5, 5 )
  gfx.drawText( "Crank change: " .. tostring( crankChangeDegrees ), 5, 25 )
  gfx.drawText( "Cosine: " .. tostring( cosine ), 5, 45 )
  gfx.drawText( "Sine: " .. tostring( sine ), 5, 65 )

  gfx.drawLine( x1, y1, x2, y2 )
  gfx.drawCircleAtPoint( x2, y2, 5 )
  gfx.drawCircleAtPoint( x1, y1, radius )
end
