-- Basic Playdate test - fonts
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'

print( "Example ~ Fonts ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

fonts = {
  { name = "Quickboot-7-Medium", font = gfx.font.new( "Quickboot-7-Medium" ) },
  { name = "Asheville-Sans-24-Light", font = gfx.font.new( "Asheville-Sans-24-Light" ) },
  { name = "font-Bitmore-Medieval-Outlined", font = gfx.font.new( "font-Bitmore-Medieval-Outlined" ) },
  { name = "font-pedallica-fun-16", font = gfx.font.new( "font-pedallica-fun-16" ) },
  { name = "font-pixieval-large-black", font = gfx.font.new( "font-pixieval-large-black" ) },
  { name = "font-Cuberick-Bold", font = gfx.font.new( "font-Cuberick-Bold" ) },
  { name = "Newsleak-Serif-Bold", font = gfx.font.new( "Newsleak-Serif-Bold" ) },
  { name = "Nontendo-Bold", font = gfx.font.new( "Nontendo-Bold" ) },
  { name = "Oklahoma-Bold", font = gfx.font.new( "Oklahoma-Bold" ) },
  { name = "font-rains-1x", font = gfx.font.new( "font-rains-1x" ) },
  { name = "Roobert-10-Bold", font = gfx.font.new( "Roobert-10-Bold" ) },
  { name = "Sasser-Slab", font = gfx.font.new( "Sasser-Slab" ) },
}


function playdate.update()

  x = 10
  y = 10
  inc = 25

  for key, data in pairs( fonts ) do
    gfx.setFont( data["font"] )
    gfx.drawText( data["name"], x, y )
    y += inc
    
    if ( y >= 230 ) then
      y = 100
      x += 200
    end
  end
  
  
end
