-- Basic Playdate test - detect keypresses and display text to the screen
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'

print( "Example ~ Detecting a keypress and displaying text to the screen ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

text = "nothing"

function playdate.leftButtonDown()    text = "left PRESS"     end
function playdate.leftButtonUp()      text = "left RELEASE"   end

function playdate.rightButtonDown()   text = "right PRESS"    end
function playdate.rightButtonUp()     text = "right RELEASE"  end

function playdate.upButtonDown()      text = "up PRESS"       end
function playdate.upButtonUp()        text = "up RELEASE"     end

function playdate.downButtonDown()    text = "down PRESS"     end
function playdate.downButtonUp()      text = "down RELEASE"   end

function playdate.AButtonDown()       text = "A PRESS"        end
function playdate.AButtonUp()         text = "A RELEASE"      end

function playdate.BButtonDown()       text = "B PRESS"        end
function playdate.BButtonUp()         text = "B RELEASE"      end

function playdate.update()
  
  gfx.clear( gfx.kColorWhite )
  gfx.drawTextAligned( text, 400/2, 240/2, kTextAlignment.center )
  
end
