import 'CoreLibs/graphics.lua'
import 'CoreLibs/timer.lua'

print( "Example ~ SaveLoad test ~ RWSS" )

local gfx = playdate.graphics

gamedata = {
  saveCount = 0,
  name = "Rachel"
}

print( "gamedata:", gamedata )

function SaveData()
  playdate.datastore.write( gamedata )
end

function LoadData()
  result = playdate.datastore.read() -- "data" by default

  if ( result ~= nil ) then
    gamedata = result
  end
end

LoadData()

gamedata["saveCount"] += 1

SaveData()

function playdate.update()
  gfx.clear( gfx.kColorWhite )

  gfx.drawText( "saveCount: " .. tostring( gamedata.saveCount ), 100, 100 )
  gfx.drawText( "name: " .. gamedata.name, 100, 150 )

  playdate.timer.updateTimers()
end
