-- Basic Playdate test - Draw shapes to the screen
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'

print( "Example ~ Draw shapes ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

function playdate.update()
  
  gfx.clear( gfx.kColorWhite )
  
  -- Colors:
  -- playdate.graphics.kColorBlack
  -- playdate.graphics.kColorWhite
  -- playdate.graphics.kColorClear
  -- playdate.graphics.kColorXOR

  gfx.setColor( gfx.kColorBlack )
  gfx.drawLine( 50, 50, 100, 70 )           -- playdate.graphics.drawLine(x1, y1, x2, y2)
  gfx.drawRect( 100, 100, 100, 50 )         -- playdate.graphics.drawRect(x, y, w, h) 
  
  gfx.setColor( gfx.kColorXOR )
  gfx.fillRect( 75, 75, 50, 100 )           -- playdate.graphics.fillRect(x, y, width, height)
  gfx.drawRoundRect( 200, 10, 60, 60, 15 )  -- playdate.graphics.drawRoundRect(x, y, w, h, radius)
  
  gfx.drawCircleAtPoint( 300, 120, 30 )     -- playdate.graphics.drawCircleAtPoint(x, y, radius)
  gfx.drawCircleInRect( 350, 150, 30, 30 )  -- playdate.graphics.drawCircleInRect(x, y, width, height)
  gfx.drawEllipseInRect( 300, 150, 90, 30 ) -- playdate.graphics.drawEllipseInRect(x, y, width, height, [startAngle, endAngle]) 
  
end
