-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

-- Documentation: https://sdk.play.date/1.11.1/Inside%20Playdate.html#_filesystem_operations

import 'CoreLibs/graphics.lua'

print( "Example ~ Filesystem ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

yOffset = 0

function playdate.update()
  gfx.clear( gfx.kColorWhite )
  
  paths = {
    { path = "."      , files = playdate.file.listFiles( "." ) },
    { path = "../"    , files = playdate.file.listFiles( "../" ) },
    { path = "../../" , files = playdate.file.listFiles( "../../" ) },
    { path = "~"      , files = playdate.file.listFiles( "~" ) },
  }
  
  x = 10
  y = 0
  
  if ( yOffset > 0 ) then yOffset = 0 end

  for index, pathData in pairs( paths ) do
    if ( pathData["files"] ~= nil ) then
      gfx.drawText( "PATH: " .. pathData["path"], x, y + yOffset )
      y += 20
      
      for fileKey, file in pairs( pathData["files"] ) do
        gfx.drawText( file, x + 5, y + yOffset )
        y += 20
      end
      
      y += 20
    end
  end

end

function playdate.upButtonDown()      yOffset += 50   end
function playdate.upButtonUp()           end
function playdate.downButtonDown()    yOffset -= 50   end
function playdate.downButtonUp()         end
