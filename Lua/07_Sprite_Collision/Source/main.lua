-- Basic Playdate test - Use PlayDate sprite object
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Wil Sha Singh

-- Using PlayDate sprite:
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.sprite

-- There is also an ImageTable object available:
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.imagetable
-- Will use these in a future example

import 'CoreLibs/graphics.lua'
import 'CoreLibs/sprites'         -- DON'T FORGET THIS IMPORT!!

print( "Example ~ Using PlayDate Sprite class ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

--screenBoundaries = {
  --minX : 0,
  --maxX : 400,
  --minY : 0,
  --maxY : 240
--}

-- Images:
imageList = {
  cat = gfx.image.new( "images/cat.png" ),
  stone = gfx.image.new( "images/stone.png" ),
}

cat = gfx.sprite.new( imageList["cat"] )
cat:moveTo( 200, 120 )
cat:add()

block = gfx.sprite.new( imageList["stone"] )
block:moveTo( 50, 50 )
block:add()

function playdate.update()
  gfx.clear( gfx.kColorWhite )
    
  gfx.sprite.update() -- Update ALL sprite objects
  
  -- Use .. to concatenate strings in Lua
  str = "Collision:"
  gfx.drawTextAligned( "COLLISION TEST", 400/2, 10, kTextAlignment.center )
  gfx.drawTextAligned( str, 400/2, 25, kTextAlignment.center )
  
end
