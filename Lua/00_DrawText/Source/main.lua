-- Basic Playdate test - draw text to screen
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'

print( "Example ~ Basic text drawing example ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

function playdate.update()

  gfx.drawText( "Hello!", 100, 100 )
  gfx.drawTextAligned( "World!", 400/2, 240/2, kTextAlignment.center )
  
end
