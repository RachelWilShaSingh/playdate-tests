import 'CoreLibs/graphics.lua'
import 'CoreLibs/timer.lua'

print( "Example ~ Timer test ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

variableThing = "nothing"
totalSeconds = 0
  
countdownTimer = playdate.timer.new( 1000, function()  
  variableThing = "countdownTimer BEEP"
end )

function SecondCounter()
  secondTimer = playdate.timer.new( 1000, SecondCounter )
  totalSeconds += 1
end

secondTimer = playdate.timer.new( 1000, SecondCounter )

function playdate.update()
  gfx.clear( gfx.kColorWhite )
  
  gfx.drawText( "timerthing: " .. countdownTimer.currentTime, 5, 10 )
  gfx.drawText( "variableThing: " .. variableThing, 5, 30 )
  gfx.drawText( "totalSeconds: " .. totalSeconds, 5, 50 )
  
  playdate.timer.updateTimers()
end
