-- Basic Playdate test - Use PlayDate sprite object along with image table and animator
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Wil Sha Singh

-- Sprite:        https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.sprite
-- Image table:   https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.imagetable
-- Animator:      https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.animation.loop

import 'CoreLibs/graphics.lua'
import 'CoreLibs/sprites'         -- DON'T FORGET THIS IMPORT!!
import 'CoreLibs/animation'       -- DON'T FORGET THIS IMPORT!!

print( "Example ~ Using PlayDate Sprite class and Animator ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

-- Image table:
-- My image file is "robot-table-20-20.png", so loading the image table should be "robot"
imageTable, result = gfx.imagetable.new( "robot" )
if ( result == nil ) then   print( "IMAGE TABLE LOADED", imageTable )
else                        print( "ERROR:", result )
end

-- Animator:
animator = gfx.animation.loop.new( 500, imageTable )

-- Image and sprite:
robotSprite = gfx.sprite.new() 
robotSprite:moveTo( 100, 100 )  -- Sets x and y

-- I'm getting an error: `animator` must be of type CoreLibs/animator
--robotSprite:setAnimator( animator )                                     -- playdate.graphics.sprite:setAnimator(animator, [moveWithCollisions, [removeOnCollision]])

robotSprite:add()

function playdate.update()
  gfx.clear( gfx.kColorWhite )
    
  --gfx.sprite.update() -- Update ALL sprite objects
  
  -- Sprite movement
  --robotSprite:moveBy( 1, 1 )
  --if ( robotSprite.x > 400 ) then robotSprite:moveTo( 0, robotSprite.y ) end
  --if ( robotSprite.y > 240 ) then robotSprite:moveTo( robotSprite.x, 0 ) end
  
  imageTable:getImage(1):draw( 0, 0 )
  animator:draw( 100, 100 )
  
  
end
