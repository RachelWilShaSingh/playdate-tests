-- Basic Playdate test - draw image to screen
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'

print( "Example ~ Drawing a basic image to the screen ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

x = 50
y = 50

logo = gfx.image.new( "images/moosadee-logo.png" )

function playdate.update()
  gfx.clear( gfx.kColorWhite )

  x = x + 1
  if ( x > 400 ) then x = -80 end

  y = y + 1
  if ( y > 240 ) then y = -85 end

  logo:draw( x, y )

  -- Use .. to concatenate strings in Lua
  str = tostring( x ) .. ", " .. tostring( y )
  gfx.drawTextAligned( "IMAGE TEST", 400/2, 10, kTextAlignment.center )
  gfx.drawTextAligned( str, 400/2, 25, kTextAlignment.center )

end
