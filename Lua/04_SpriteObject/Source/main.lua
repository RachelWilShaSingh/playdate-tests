-- Basic Playdate test - Use PlayDate sprite object
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Wil Sha Singh

-- Using PlayDate sprite:
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.sprite

-- There is also an ImageTable object available:
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.imagetable
-- Will use these in a future example

import 'CoreLibs/graphics.lua'
import 'CoreLibs/sprites'         -- DON'T FORGET THIS IMPORT!!

print( "Example ~ Using PlayDate Sprite class ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

-- Image:
logo = gfx.image.new( "images/moosadee-logo.png" )

-- Image and sprite:
robotImage = gfx.image.new( "images/robot.png" )
robotSprite = gfx.sprite.new( robotImage ) -- Create a sprite object
robotSprite:moveTo( 100, 100 )  -- Sets x and y
robotSprite:setSize( 20, 20 ) -- Sets width and height
robotSprite:setVisible( true )
robotSprite:add() -- add to display list

x = 100
y = 100

function playdate.update()
  gfx.clear( gfx.kColorWhite )
    
  gfx.sprite.update() -- Update ALL sprite objects
  
  -- Sprite rotation
  robotSprite:setRotation( robotSprite:getRotation() + 1 )
  
  -- Sprite movement
  robotSprite:moveBy( 1, 1 )
  if ( robotSprite.x > 400 ) then robotSprite:moveTo( 0, robotSprite.y ) end
  if ( robotSprite.y > 240 ) then robotSprite:moveTo( robotSprite.x, 0 ) end
    
  -- Use .. to concatenate strings in Lua
  str = "Position: " .. tostring( robotSprite.x ) .. ", " .. tostring( robotSprite.y )
  gfx.drawTextAligned( "IMAGE TEST", 400/2, 10, kTextAlignment.center )
  gfx.drawTextAligned( str, 400/2, 25, kTextAlignment.center )
  
  -- Draw logo image
  logo:draw( 0, 0 )
  
end
