-- Basic Playdate test - Play sound with sampleplayer
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-sound.sampleplayer
-- Rachel Singh

import 'CoreLibs/graphics.lua'

print( "Example ~ Playing sound with sampleplayer ~ RWSS" )

local gfx = playdate.graphics
local snd = playdate.sound

playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

sound1 = snd.sampleplayer.new( "audio/sound1.wav" )
sound2 = snd.sampleplayer.new( "audio/sound2.wav" )

text = ""

function playdate.AButtonUp()         
  sound1:play()
  text = "Play sound 1"
end

function playdate.BButtonUp()
  sound2:play()
  text = "Play sound 2"
end

function playdate.update()
  
  gfx.clear( gfx.kColorWhite )
  gfx.drawTextAligned( "Press the A button or B button to hear a sound", 400/2, 240/2, kTextAlignment.center )
  gfx.drawTextAligned( text, 400/2, 240/2 + 20, kTextAlignment.center )
  
end
