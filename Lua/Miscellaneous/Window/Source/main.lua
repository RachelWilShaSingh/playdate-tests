import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

window = gfx.image.new( "images/window.png" )
x = 0
y = 0
horiz = 1
vert = 1

text = "Hello, world!"

compy = gfx.image.new( "images/mycompy.png" )
music = gfx.image.new( "images/music.png" )
chat  = gfx.image.new( "images/chat.png" )
window2 = gfx.image.new( "images/window2.png" )


function playdate.update()

  x += horiz
  y += vert
  
  if ( x > 400-97 or x < 0 ) then horiz = -horiz end
  if ( y > 240-73 or y < 0 ) then vert = -vert end
  
  gfx.clear( gfx.kColorWhite )
  
  compy:draw( 5, 5 )
  gfx.drawTextAligned( "Compy", 5, 45 )
  
  music:draw( 5, 75 )
  gfx.drawTextAligned( "Music", 5, 115 )
  
  chat:draw( 5, 145 )
  gfx.drawTextAligned( "Chat", 5, 185 )
  
  window2:draw( 75, 20 )
  
  window:draw( x, y )  
  gfx.drawTextAligned( text, x + 5, y + 20 )
  
  
end
