-- Basic Playdate test - draw image in spritesheet and animate it
-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

-- There is also an ImageTable object available:
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.imagetable
-- As well as a Sprite object for PlayDate code:
-- https://sdk.play.date/1.9.0/Inside%20Playdate.html#C-graphics.sprite
-- Will use these in a future example

import 'CoreLibs/graphics.lua'

print( "Example ~ Animating an image from a spritesheet ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

robotSprite = gfx.image.new( "images/robot.png" )
robotDirection = 0
robotDirectionMax = 3
robotFrame = 0
robotFrameMax = 2
robotAnimateSpeed = 0.1
robotWH = 20 -- 20 pixels width/height
robotX = 200
robotY = 50

function playdate.update()
  gfx.clear( gfx.kColorWhite )

  robotFrame = robotFrame + robotAnimateSpeed
  if ( robotFrame >= robotFrameMax ) then
    robotFrame = 0
    robotDirection = robotDirection + 1
    if ( robotDirection > robotDirectionMax ) then
      robotDirection = 0
    end
  end
    
  -- playdate.graphics.image:draw(x, y, [flip, [sourceRect]]) 
  robotSprite:draw( robotX, robotY, gfx.kImageUnflipped, math.floor( robotFrame ) * robotWH, robotDirection * robotWH, robotWH, robotWH )
  
  -- Use .. to concatenate strings in Lua
  str = "Direction: " .. tostring( robotDirection ) .. ", Frame: " .. tostring( robotFrame )
  gfx.drawTextAligned( "IMAGE TEST", 400/2, 10, kTextAlignment.center )
  gfx.drawTextAligned( str, 400/2, 25, kTextAlignment.center )
  
  
end
