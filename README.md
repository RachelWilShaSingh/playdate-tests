# playdate-tests

Test code for PlayDate

## Playdate SDK and simulator

Get the SDK and simulator here: https://play.date/dev/

## License

My example code here is public domain you can use snippets or whole files for whatever you want.
