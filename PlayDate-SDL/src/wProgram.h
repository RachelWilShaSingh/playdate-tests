#ifndef _W_PROGRAM_H
#define _W_PROGRAM_H

//#define PD
#define SDL

#if defined( SDL )
#include <SDL2/SDL.h>
#elif defined( PD )
#include "pd_api.h"
#endif

struct {
    int screenWidth;
    int screenHeight;

#if defined( SDL )
    SDL_Window* window;
    SDL_Surface* screen;
#elif defined( PD )
    PlaydateAPI* playdate;
    LCDFont* font;
#endif
} Program;

void Setup();
void Cleanup();

#ifdef SDL
void Setup_SDL();
void Cleanup_SDL();
#endif

#ifdef PD
void Setup_PD( PlaydateAPI* pd );
void Cleanup_PD();
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg );
int update( PlaydateAPI* pd );   // Must return int
#endif

#endif
