#include "wProgram.h"
#include <stdio.h>

void Setup()
{
    printf( "* Setup() \n" );

    Program.screenWidth = 400;
    Program.screenHeight = 240;

#if defined( SDL )
    Setup_SDL();
#elif defined( PD )
    // Setup_PD(); // This is called by the eventHandler.
#endif
}

void Cleanup()
{
    printf( "* Cleanup() \n" );

#if defined( SDL )
    Cleanup_SDL();
#elif defined( PD )
    Cleanup_PD();
#endif
}

#ifdef SDL
void Setup_SDL()
{
    printf( "* Setup_SDL() \n" );

    Program.window = NULL;
    Program.screen = NULL;

    // Initialize SDL
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
        printf( "Error initializing SDL! %s \n", SDL_GetError() );
        return;
    }

    // Create window
    Program.window = SDL_CreateWindow( "Wrapper test",
                                      SDL_WINDOWPOS_UNDEFINED,
                                      SDL_WINDOWPOS_UNDEFINED,
                                      Program.screenWidth,
                                      Program.screenHeight,
                                      SDL_WINDOW_SHOWN );

    if ( Program.window == NULL )
    {
        printf( "Error creating window! %s \n", SDL_GetError() );
        return;
    }

    Program.screen = SDL_GetWindowSurface( Program.window );

    SDL_Delay( 2000 );
}

void Cleanup_SDL()
{
    printf( "* Cleanup_SDL() \n" );

    SDL_DestroyWindow( Program.window );
    SDL_Quit();
}
#endif

#ifdef PD
void Setup_PD( PlaydateAPI* pd )
{
    printf( "* Setup_PD() \n" );

    Program.playdate = pd;

    // Set a callback - the Update function gets called each cycle
    pd->system->setUpdateCallback( update, Program.playdate );
    
    
    // Load a font file
    const char* err;
    const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";
    Program.font = pd->graphics->loadFont( fontpath, &err );
    if ( Program.font == NULL )
    {
      pd->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, err );
    }
}

void Cleanup_PD()
{
    printf( "* Cleanup_PD() \n" );

}

int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
    printf( "* eventHandler() \n" );

    if ( event == kEventInit )
    {
        Setup_PD( pd );
    }
}

int update( PlaydateAPI* pd ) // Must return int
{
    pd->graphics->clear( kColorWhite );
    pd->system->drawFPS( 0,0 );
    pd->graphics->setFont( Program.font );
    pd->graphics->drawText( "Hello World!", strlen("Hello World!"), kASCIIEncoding, 100, 100 );
    
    return 1;
}
#endif
