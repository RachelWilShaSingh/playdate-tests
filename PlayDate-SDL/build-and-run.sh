# https://sdk.play.date/1.9.0/Inside%20Playdate%20with%20C.html

compilerPath='/home/wilsha/PlaydateSDK-1.9.0/bin/pdc'
simulatorPath='/home/wilsha/PlaydateSDK-1.9.0/bin/PlaydateSimulator'
sourcePath='./src'
outputPath='./build/pdex.so'

# pdc [sourcepath] [outputpath]

echo "Compiler path:  $compilerPath"
echo "Simulator path: $simulatorPath"
echo "Source path:    $sourcePath"
echo "Output path:    $outputPath"
echo ""

echo "Compile source for simulator..."
make simulator

#echo "Compile source for as .pdx..."
#make pdc

#echo "Compile source for device..."
#make bin

echo "Opening simulator..."
$simulatorPath $outputPath
