#include "pan_system.h"
#include "Structures.h"

#include <stdio.h>
#include <stdlib.h>

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#endif

void Pan_Setup( struct SYSTEM* sys )
{
  //printf( "FUNCTION BEGIN: %s (%s) \n", __func__, __FILE__ );
  sys->fatalError = false;
  sys->wantToQuit = false;

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)

#elif defined(TARGET_SDL)
  sys->sdl_window = NULL;
  sys->sdl_surface = NULL;

  if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
      printf( "%s:%i ERROR: SDL_Init: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_Init successful \n", __FILE__, __LINE__ ); }

  if ( TTF_Init() == -1 )
    {
      printf( "%s:%i ERROR: TTF_Init: %s \n", __FILE__, __LINE__, TTF_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i TTF_Init successful \n", __FILE__, __LINE__ ); }

  sys->sdl_window = SDL_CreateWindow( "Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 400, 240, SDL_WINDOW_SHOWN );
  if ( sys->sdl_window == NULL )
    {
      printf( "%s:%i ERROR: SDL_CreateWindow: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_CreateWindow successful \n", __FILE__, __LINE__ ); }

  sys->sdl_surface = SDL_GetWindowSurface( sys->sdl_window );
  if ( sys->sdl_surface == NULL )
    {
      printf( "%s:%i ERROR: SDL_GetWindowSurface: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_GetWindowSurface successful \n", __FILE__, __LINE__ ); }

  int imgFlags = IMG_INIT_PNG;
  if ( !( IMG_Init( imgFlags ) & imgFlags ) )
    {
      printf( "%s:%i ERROR: IMG_Init: %s \n", __FILE__, __LINE__, IMG_GetError() );
      sys->fatalError = true;
    }

  sys->input.buttonLeft    = INACTIVE;
  sys->input.buttonRight   = INACTIVE;
  sys->input.buttonUp      = INACTIVE;
  sys->input.buttonDown    = INACTIVE;
  sys->input.buttonActionL = INACTIVE;
  sys->input.buttonActionR = INACTIVE;
#endif
  //printf( "FUNCTION END: %s (%s) \n", __func__, __FILE__ );
}

void Pan_Teardown( struct SYSTEM* sys )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_DestroyWindow( sys->sdl_window );
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
#endif
}

void Pan_ClearScreen( struct SYSTEM* sys )
{
  //printf( "FUNCTION BEGIN: %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  sys->playdate->graphics->clear( kColorWhite );
#elif defined(TARGET_SDL)
  SDL_FillRect( sys->sdl_surface, NULL, SDL_MapRGB( sys->sdl_surface->format, 0xCC, 0xCC, 0xCC ) );
#endif
  //printf( "FUNCTION END: %s (%s) \n", __func__, __FILE__ );
}

void Pan_DrawScreen( struct SYSTEM* sys )
{
  //printf( "FUNCTION BEGIN: %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_UpdateWindowSurface( sys->sdl_window );
#endif
  //printf( "FUNCTION END: %s (%s) \n", __func__, __FILE__ );
}

void Pan_HandleInput( struct SYSTEM* sys )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_Event e;
  while ( SDL_PollEvent( &e ) )
    {
      if ( e.type == SDL_QUIT ) {
        sys->wantToQuit = true;
      }
      else if ( e.type == SDL_KEYDOWN )
        {
          // INACTIVE -> BEGIN_PUSH -> HELD_DOWN
          if      ( e.key.keysym.sym == SDLK_w && sys->input.buttonUp      == INACTIVE   ) { sys->input.buttonUp    = BEGIN_PUSH;   }
          else if ( e.key.keysym.sym == SDLK_w && sys->input.buttonUp      == BEGIN_PUSH ) { sys->input.buttonUp    = HELD_DOWN;    }

          if      ( e.key.keysym.sym == SDLK_s && sys->input.buttonDown    == INACTIVE   ) { sys->input.buttonDown  = BEGIN_PUSH;   }
          else if ( e.key.keysym.sym == SDLK_s && sys->input.buttonDown    == BEGIN_PUSH ) { sys->input.buttonDown  = HELD_DOWN;    }

          if      ( e.key.keysym.sym == SDLK_a && sys->input.buttonLeft    == INACTIVE   ) { sys->input.buttonLeft  = BEGIN_PUSH;   }
          else if ( e.key.keysym.sym == SDLK_a && sys->input.buttonLeft    == BEGIN_PUSH ) { sys->input.buttonLeft  = HELD_DOWN;    }

          if      ( e.key.keysym.sym == SDLK_d && sys->input.buttonRight   == INACTIVE   ) { sys->input.buttonRight = BEGIN_PUSH;   }
          else if ( e.key.keysym.sym == SDLK_d && sys->input.buttonRight   == BEGIN_PUSH ) { sys->input.buttonRight = HELD_DOWN;    }

          if      ( e.key.keysym.sym == SDLK_j && sys->input.buttonActionL == INACTIVE   ) { sys->input.buttonActionL = BEGIN_PUSH; }
          else if ( e.key.keysym.sym == SDLK_j && sys->input.buttonActionL == BEGIN_PUSH ) { sys->input.buttonActionL = HELD_DOWN;  }

          if      ( e.key.keysym.sym == SDLK_k && sys->input.buttonActionR == INACTIVE   ) { sys->input.buttonActionR = BEGIN_PUSH; }
          else if ( e.key.keysym.sym == SDLK_k && sys->input.buttonActionR == BEGIN_PUSH ) { sys->input.buttonActionR = HELD_DOWN;  }
        }
      else if ( e.type == SDL_KEYUP )
        {
          // HELD_DOWN -> BEGIN_RELEASE -> INACTIVE
          if      ( e.key.keysym.sym == SDLK_w && ( sys->input.buttonUp      == HELD_DOWN || sys->input.buttonUp      == BEGIN_PUSH )     ) { sys->input.buttonUp      = BEGIN_RELEASE; }
          if      ( e.key.keysym.sym == SDLK_s && ( sys->input.buttonDown    == HELD_DOWN || sys->input.buttonDown    == BEGIN_PUSH )     ) { sys->input.buttonDown    = BEGIN_RELEASE; }
          if      ( e.key.keysym.sym == SDLK_a && ( sys->input.buttonLeft    == HELD_DOWN || sys->input.buttonLeft    == BEGIN_PUSH )     ) { sys->input.buttonLeft    = BEGIN_RELEASE; }
          if      ( e.key.keysym.sym == SDLK_d && ( sys->input.buttonRight   == HELD_DOWN || sys->input.buttonRight   == BEGIN_PUSH )     ) { sys->input.buttonRight   = BEGIN_RELEASE; }
          if      ( e.key.keysym.sym == SDLK_j && ( sys->input.buttonActionL == HELD_DOWN || sys->input.buttonActionL == BEGIN_PUSH )     ) { sys->input.buttonActionL = BEGIN_RELEASE; }
          if      ( e.key.keysym.sym == SDLK_k && ( sys->input.buttonActionR == HELD_DOWN || sys->input.buttonActionR == BEGIN_PUSH )     ) { sys->input.buttonActionR = BEGIN_RELEASE; }
        }
    }
#endif
}

void Pan_Update( struct SYSTEM* sys )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  if ( sys->input.buttonUp       == BEGIN_RELEASE ) { sys->input.buttonUp      = INACTIVE; }
  if ( sys->input.buttonDown     == BEGIN_RELEASE ) { sys->input.buttonDown    = INACTIVE; }
  if ( sys->input.buttonLeft     == BEGIN_RELEASE ) { sys->input.buttonLeft    = INACTIVE; }
  if ( sys->input.buttonRight    == BEGIN_RELEASE ) { sys->input.buttonRight   = INACTIVE; }
  if ( sys->input.buttonActionL  == BEGIN_RELEASE ) { sys->input.buttonActionL = INACTIVE; }
  if ( sys->input.buttonActionR  == BEGIN_RELEASE ) { sys->input.buttonActionR = INACTIVE; }
#endif
}

struct Font Pan_TryLoadFont( struct SYSTEM* sys, const char* path )
{
  struct Font font;

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  const char* err;
  font.font = sys->playdate->graphics->loadFont( path, &err );
  if ( font.font == NULL )
    {
      sys->playdate->system->error( "%s:%i loadFont %s: %s", __FILE__, __LINE__, path, err );
    }
  //printf( "[TryLoadFont] LCDFont address is: %p \n", (void*)font.font );
#elif defined(TARGET_SDL)
  // TODO: Need to specify size
  font.font = TTF_OpenFont( path, 28 );
  if ( font.font == NULL )
    {
      printf( "%s:%i ERROR: TTF_OpenFont:  %s: %s", __FILE__, __LINE__, path, TTF_GetError() );
    }
#endif
  return font;
}

void Pan_FreeFont( struct Font* font )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  TTF_CloseFont( font->font );
#endif
}

void Pan_DrawText( struct SYSTEM* sys, struct Font* font, const char* text )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  sys->playdate->graphics->setFont( font->font );
  sys->playdate->graphics->drawText( text, strlen( text ), kASCIIEncoding, 10, 10 );
#elif defined(TARGET_SDL)
  // TODO: Allow different text colors
  SDL_Color textColor = { 0, 0, 0 };
  SDL_Surface* textSurface = NULL;
  textSurface = TTF_RenderText_Solid( font->font, text, textColor );
  SDL_BlitSurface( textSurface, NULL, sys->sdl_surface, NULL );
  // TODO: This should be handled better
  SDL_FreeSurface( textSurface );
#endif
}

struct Png Pan_TryLoadPng( struct SYSTEM* sys, const char* path )
{
  struct Png png;
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  const char* err;
  png.image = sys->playdate->graphics->loadBitmap( path, &err );
  if ( png.image == NULL )
    {
      sys->playdate->system->error( "%s:%i loadBitmap %s: %s", __FILE__, __LINE__, path, err );
    }
#elif defined(TARGET_SDL)
  SDL_Surface* load = IMG_Load( path );
  if ( load == NULL )
    {
      printf( "%s:%i ERROR: IMG_Load:  %s: %s", __FILE__, __LINE__, path, IMG_GetError() );
    }
  else
    {
      png.image = SDL_ConvertSurface( load, sys->sdl_surface->format, 0 );
      if ( png.image == NULL )
        {
          printf( "%s:%i ERROR: SDL_ConvertSurface:  %s: %s", __FILE__, __LINE__, path, SDL_GetError() );
        }
      SDL_FreeSurface( load );

      SDL_SetColorKey( png.image, SDL_TRUE, SDL_MapRGB( png.image->format, 0xFF, 0, 0xFF ) );
    }
#endif

  return png;
}

void Pan_FreePng( struct Png* png )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_FreeSurface( png->image );
#endif
}

void Pan_DrawPng( struct SYSTEM* sys, struct Png* png, int x, int y )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  sys->playdate->graphics->drawBitmap( png->image, x, y, 0 );
#elif defined(TARGET_SDL)
  Rect pos;
  pos.x = x;
  pos.y = y;
  SDL_BlitSurface( png->image, NULL, sys->sdl_surface, &pos );
#endif
}
