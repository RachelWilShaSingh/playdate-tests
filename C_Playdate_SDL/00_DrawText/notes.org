# -*- mode: org -*-

* Build notes

Currently trying to get a #define set only when building for playdate; tried CFLAGS += -DPDPLATFORM in the makefile, tried including a .h or .c file that has =#define PDPLATFORM=, but it doesn't seem to work.
Tester code:

  #ifdef PDPLATFORM
  printf( "\n PDPLATFORM \n" );
  #else
  printf( "\n NOPLAT \n" );
  #endif


The common.mk has some defines on its own, and from the compile output we can see =gcc -g -shared -fPIC -lm -DTARGET_SIMULATOR=1 -DTARGET_EXTENSION=1= , and I need to try for when building for the actual system.
TARGET_SIMULATOR can be used to check for if it's playdate, or TARGET_PLAYDATE.
