// Throwing all the SDL stuff in here while I remember how to do this xD
// Thanks to LazyFoo tutorials <3

#include <stdio.h>

#include "Functions.h"
#include "Structures.h"

int main()
{
  struct SYSTEM sys;

  // Using the wrapped structures version:

  Setup( &sys );
  if ( sys.fatalError == true )
    {
      printf( "FATAL ERROR! \n" );
      return 1;
    }

  while ( sys.wantToQuit == false )
    {
      HandleInput( &sys );
      Update( &sys );
      Draw( &sys );
    }

  Teardown( &sys );


  // All of the SDL program in this one file version:
  /*
#ifdef TARGET_SDL
  printf( "\n SDL \n" );
#endif
  SDL_Window* window = NULL;
  SDL_Surface* surface = NULL;

  if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
      printf( "SDL INIT ERROR %s \n", SDL_GetError() );
      return 1;
    }

  if ( TTF_Init() == -1 )
    {
      printf( "TTF INIT ERROR %s \n", TTF_GetError() );
      return 2;
    }

  window = SDL_CreateWindow( "Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 400, 200, SDL_WINDOW_SHOWN );
  if ( window == NULL )
    {
      printf( "COULD NOT INIT WINDOW %s \n", SDL_GetError() );
      return 3;
    }


  TTF_Font* font = NULL;
  font = TTF_OpenFont( "src/Moosomnia.ttf", 28 );
  if ( font == NULL )
    {
      printf( "COULD NOT LOAD FONT  %s \n", TTF_GetError() );
      return 4;
    }

  SDL_Surface* textSurface = NULL;
  const char* text = "Hello!";
  SDL_Color textColor = { 0, 0, 0 };
  textSurface = TTF_RenderText_Solid( font, text, textColor );


  surface = SDL_GetWindowSurface( window );
  SDL_FillRect( surface, NULL, SDL_MapRGB( surface->format, 0xFF, 0x00, 0xFF ) );
  SDL_BlitSurface( textSurface, NULL, surface, NULL );

  SDL_UpdateWindowSurface( window );

  SDL_Event e;
  bool quit = false;
  while ( !quit )
    {
      while ( SDL_PollEvent( &e ) )
        {
          if ( e.type == SDL_QUIT ) {
            quit = true;
          }
        }
    }

  // Free the data
  SDL_FreeSurface( textSurface );
  TTF_CloseFont( font );
  //SDL_DestroyRenderer( renderer );
  SDL_DestroyWindow( window );

  TTF_Quit();
  SDL_Quit();
  */

  return 0;
}
