#ifndef _STRUCTURES
#define _STRUCTURES

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#endif

#include <stdbool.h>

struct SYSTEM {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_Window* sdl_window;
  SDL_Surface* sdl_surface;
#endif
  bool fatalError;
  bool wantToQuit;
};

struct Font {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  LCDFont* font;
#elif defined(TARGET_SDL)
  TTF_Font* font;
#endif
};

#endif
