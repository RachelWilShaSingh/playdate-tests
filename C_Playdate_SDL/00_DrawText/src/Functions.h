#ifndef _FUNCTIONS
#define _FUNCTIONS

#include <stdbool.h>

#include "Structures.h"

void Setup( struct SYSTEM* sys );
void Teardown( struct SYSTEM* sys );
void HandleInput( struct SYSTEM* sys );
void Update( struct SYSTEM* sys );
void Draw( struct SYSTEM* sys );

#endif
