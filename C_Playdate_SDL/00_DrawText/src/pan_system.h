#ifndef _PAN_SYSTEM
#define _PAN_SYSTEM

#include <stdbool.h>

#include "Structures.h"

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#endif

void Pan_Setup( struct SYSTEM* sys );
void Pan_Teardown( struct SYSTEM* sys );

void Pan_ClearScreen( struct SYSTEM* sys );
void Pan_DrawScreen( struct SYSTEM* sys );

void Pan_HandleInput( struct SYSTEM* sys );

void Pan_DrawText( struct SYSTEM* sys, struct Font* font, const char* text );

struct Font Pan_TryLoadFont( const char* path );

#endif
