// https://gitlab.com/RachelWilShaSingh/playdate-tests
// Rachel Singh

#include <stdio.h>
#include <stdlib.h>

#include "Structures.h"
#include "Functions.h"
#include "pan_system.h"

struct Font testFont;

void Setup( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_Setup( sys );

  // Different font depending on version so need this nonagnostic version unfortunately.
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  testFont = Pan_TryLoadFont( "/System/Fonts/Asheville-Sans-14-Bold.pft" );
  printf( "[Setup] LCDFont address is: %p \n", (void*)testFont.font );
#elif defined(TARGET_SDL)
  testFont = Pan_TryLoadFont( "Moosomnia.ttf" );
#endif
}

void Teardown( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_Teardown( sys );
}

void HandleInput( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_HandleInput( sys );
}

void Update( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
}

void Draw( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_ClearScreen( sys );
  Pan_DrawText( sys, &testFont, "Hello, world!" );
  Pan_DrawScreen( sys );
}
