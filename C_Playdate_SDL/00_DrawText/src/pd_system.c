#include "pd_system.h"
#include "pd_api.h"
#include "Functions.h"

#include <stdio.h>
#include <stdlib.h>

PlaydateAPI* pdptr = NULL;

int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  struct SYSTEM unusedSystem;
  if ( event == kEventInit )
    {
      Playdate_Setup( pd );
      Setup( &unusedSystem );
    }
  else if ( event == kEventTerminate )
    {
      Playdate_Teardown( pd );
      Teardown( &unusedSystem );
    }
}

void Playdate_Setup( PlaydateAPI* playdate )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  pdptr = playdate;
  // Set a callback - the Update function gets called each cycle
  pdptr->system->setUpdateCallback( Playdate_Update, playdate );

#ifdef TARGET_PLAYDATE
  printf( "\n TARGET_PLAYDATE \n" );
#endif

#ifdef TARGET_SIMULATOR
  printf( "\n SIMULATOR \n" );
#endif
}

void Playdate_Teardown( PlaydateAPI* playdate )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
}

static int Playdate_Update( void* pdvoid )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  struct SYSTEM unusedSystem;
  HandleInput( &unusedSystem );
  Update( &unusedSystem );
  Draw( &unusedSystem );
  return 1;
}

