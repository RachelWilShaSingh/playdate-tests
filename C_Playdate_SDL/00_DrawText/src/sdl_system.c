#include "sdl_system.h"
#include "Structures.h"

void SDL_Setup( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );

  sys->sdl_window = NULL;
  sys->sdl_surface = NULL;

  if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
      printf( "%s:%i ERROR: SDL_Init: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_Init successful \n", __FILE__, __LINE__ ); }

  if ( TTF_Init() == -1 )
    {
      printf( "%s:%i ERROR: TTF_Init: %s \n", __FILE__, __LINE__, TTF_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i TTF_Init successful \n", __FILE__, __LINE__ ); }

  sys->sdl_window = SDL_CreateWindow( "Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 400, 240, SDL_WINDOW_SHOWN );
  if ( sys->sdl_window == NULL )
    {
      printf( "%s:%i ERROR: SDL_CreateWindow: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_CreateWindow successful \n", __FILE__, __LINE__ ); }

  sys->sdl_surface = SDL_GetWindowSurface( sys->sdl_window );
  if ( sys->sdl_surface == NULL )
    {
      printf( "%s:%i ERROR: SDL_GetWindowSurface: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_GetWindowSurface successful \n", __FILE__, __LINE__ ); }
}

void SDL_Teardown( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_DestroyWindow( sys->sdl_window );
  TTF_Quit();
  SDL_Quit();
}

void SDL_ClearScreen( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_FillRect( sys->sdl_surface, NULL, SDL_MapRGB( sys->sdl_surface->format, 0xFF, 0x00, 0xFF ) );
}

void SDL_DrawScreen( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_UpdateWindowSurface( sys->sdl_window );
}

void SDL_HandleInput( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_Event e;
  while ( SDL_PollEvent( &e ) )
    {
      if ( e.type == SDL_QUIT ) {
        sys->wantToQuit = true;
      }
    }
}
