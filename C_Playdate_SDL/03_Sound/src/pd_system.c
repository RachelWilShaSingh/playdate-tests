#include "pd_system.h"
#include "pd_api.h"
#include "Functions.h"

#include <stdio.h>
#include <stdlib.h>

//PlaydateAPI* pdptr = NULL;
struct SYSTEM sys;

int eventHandler( PlaydateAPI* playdate, PDSystemEvent event, uint32_t arg )
{
  //printf( "FUNCTION BEGIN: %s (%s) \n", __func__, __FILE__ );
  if ( event == kEventInit )
    {
      Playdate_Setup( playdate );
      sys.playdate = playdate;
      Setup( &sys );
    }
  else if ( event == kEventTerminate )
    {
      Playdate_Teardown( playdate );
      Teardown( &sys );
    }
  //printf( "FUNCTION END: %s (%s) \n", __func__, __FILE__ );
}

void Playdate_Setup( PlaydateAPI* playdate )
{
  //printf( "FUNCTION BEGIN: %s (%s) \n", __func__, __FILE__ );
  // Set a callback - the Update function gets called each cycle
  playdate->system->setUpdateCallback( Playdate_Update, playdate );
  //printf( "FUNCTION END: %s (%s) \n", __func__, __FILE__ );

  printf( "Files in working directory: \n" );
  playdate->file->listfiles( ".", Playdate_DisplayFile, playdate, 0 );
}

void Playdate_DisplayFile( const char* filename, void* userdata )
{
  printf( "* %s \n", filename );
}

void Playdate_Teardown( PlaydateAPI* playdate )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
}

static int Playdate_Update( void* pdvoid )
{
  //printf( "FUNCTION BEGIN: %s (%s) \n", __func__, __FILE__ );
  PlaydateAPI* playdate = pdvoid;

  // Read inputs
  PDButtons current, pushed, released;
  playdate->system->getButtonState( &current, &pushed, &released );

  // TODO: Do this better...
  if      ( pushed & kButtonLeft   )  { sys.input.buttonLeft = BEGIN_PUSH; }
  else if ( current & kButtonLeft  )  { sys.input.buttonLeft = HELD_DOWN; }
  else if ( released & kButtonLeft )  { sys.input.buttonLeft = BEGIN_RELEASE; }
  else                                { sys.input.buttonLeft = INACTIVE; }

  if      ( pushed & kButtonRight   ) { sys.input.buttonRight = BEGIN_PUSH; }
  else if ( current & kButtonRight  ) { sys.input.buttonRight = HELD_DOWN; }
  else if ( released & kButtonRight ) { sys.input.buttonRight = BEGIN_RELEASE; }
  else                                { sys.input.buttonRight = INACTIVE; }

  if      ( pushed & kButtonUp   )    { sys.input.buttonUp = BEGIN_PUSH; }
  else if ( current & kButtonUp  )    { sys.input.buttonUp = HELD_DOWN; }
  else if ( released & kButtonUp )    { sys.input.buttonUp = BEGIN_RELEASE; }
  else                                { sys.input.buttonUp = INACTIVE; }

  if      ( pushed & kButtonDown   ) { sys.input.buttonDown = BEGIN_PUSH; }
  else if ( current & kButtonDown  ) { sys.input.buttonDown = HELD_DOWN; }
  else if ( released & kButtonDown ) { sys.input.buttonDown = BEGIN_RELEASE; }
  else                               { sys.input.buttonDown = INACTIVE; }

  if      ( pushed & kButtonB   )    { sys.input.buttonActionL = BEGIN_PUSH; }
  else if ( current & kButtonB  )    { sys.input.buttonActionL = HELD_DOWN; }
  else if ( released & kButtonB )    { sys.input.buttonActionL = BEGIN_RELEASE; }
  else                               { sys.input.buttonActionL = INACTIVE; }

  if      ( pushed & kButtonA   )    { sys.input.buttonActionR = BEGIN_PUSH; }
  else if ( current & kButtonA  )    { sys.input.buttonActionR = HELD_DOWN; }
  else if ( released & kButtonA )    { sys.input.buttonActionR = BEGIN_RELEASE; }
  else                               { sys.input.buttonActionR = INACTIVE; }


  HandleInput( &sys );
  Update( &sys );
  Draw( &sys );

  //printf( "FUNCTION END: %s (%s) \n", __func__, __FILE__ );
  return 1;
}

