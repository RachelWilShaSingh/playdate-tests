#ifndef _STRUCTURES
#define _STRUCTURES

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#endif

#include <stdbool.h>

enum InputState {
  INACTIVE = 0, BEGIN_PUSH = 1, HELD_DOWN = 2, BEGIN_RELEASE = 3
};

struct Input {
  enum InputState buttonLeft;
  enum InputState buttonRight;
  enum InputState buttonUp;
  enum InputState buttonDown;
  enum InputState buttonActionL;
  enum InputState buttonActionR;
};

struct SYSTEM {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  PlaydateAPI* playdate;
  SamplePlayer* pd_soundplayer;
  FilePlayer* pd_fileplayer;
#elif defined(TARGET_SDL)
  SDL_Window* sdl_window;
  SDL_Surface* sdl_surface;
#endif
  bool fatalError;
  bool wantToQuit;
  struct Input input;
};

struct Font {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  LCDFont* font;
#elif defined(TARGET_SDL)
  TTF_Font* font;
#endif
};

struct Png {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  LCDBitmap* image;
#elif defined(TARGET_SDL)
  SDL_Surface* image;
#endif
};

struct Sound {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  AudioSample* sound;
#elif defined(TARGET_SDL)
  Mix_Chunk* sound;
#endif
};

struct Music {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  AudioSample* music;
#elif defined(TARGET_SDL)
  Mix_Music* music;
#endif
};












#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
struct Rect
{
  int x, y, w, h;
};
#elif defined(TARGET_SDL)
#define Rect SDL_Rect
#endif

#endif
