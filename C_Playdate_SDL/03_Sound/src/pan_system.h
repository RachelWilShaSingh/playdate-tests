#ifndef _PAN_SYSTEM
#define _PAN_SYSTEM

#include <stdbool.h>

#include "Structures.h"

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#endif

void Pan_Setup( struct SYSTEM* sys );
void Pan_Teardown( struct SYSTEM* sys );

void Pan_ClearScreen( struct SYSTEM* sys );
void Pan_DrawScreen( struct SYSTEM* sys );

void Pan_HandleInput( struct SYSTEM* sys );
void Pan_Update( struct SYSTEM* sys );

struct Font Pan_TryLoadFont( struct SYSTEM* sys, const char* path );
void Pan_FreeFont( struct Font* font );
void Pan_DrawText( struct SYSTEM* sys, struct Font* font, const char* text );

struct Png Pan_TryLoadPng( struct SYSTEM* sys, const char* path );
void Pan_FreePng( struct Png* png );
void Pan_DrawPng( struct SYSTEM* sys, struct Png* png, int x, int y );

struct Sound Pan_TryLoadSound( struct SYSTEM* sys, const char* path );
void Pan_FreeSound( struct SYSTEM* sys, struct Sound* sound );
void Pan_PlaySound( struct SYSTEM* sys, struct Sound* sound );

struct Music Pan_TryLoadMusic( struct SYSTEM* sys, const char* path, const char* ext );
void Pan_FreeMusic( struct SYSTEM* sys, struct Music* music );
void Pan_PlayMusic( struct SYSTEM* sys, struct Music* music );
void Pan_StopMusic( struct SYSTEM* sys );
void Pan_PauseMusic( struct SYSTEM* sys );
void Pan_ResumeMusic( struct SYSTEM* sys );
void Pan_ToggleMusic( struct SYSTEM* sys );

#endif
