#ifndef _PDSYSTEM
#define _PDSYSTEM

#include "pd_api.h"

extern PlaydateAPI* pdptr;

int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg );

void Playdate_DisplayFile( const char* filename, void* userdata );
void Playdate_Setup( PlaydateAPI* playdate );
void Playdate_Teardown( PlaydateAPI* playdate );
static int Playdate_Update( void* pdvoid );

#endif
