#ifndef _SDLSYSTEM
#define _SDLSYSTEM

#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "Structures.h"

void SDL_Setup( struct SYSTEM* sys );
void SDL_Teardown( struct SYSTEM* sys );

void SDL_ClearScreen( struct SYSTEM* sys );
void SDL_DrawScreen( struct SYSTEM* sys );

void SDL_HandleInput( struct SYSTEM* sys );
void SDL_Update( struct SYSTEM* sys );

#endif
