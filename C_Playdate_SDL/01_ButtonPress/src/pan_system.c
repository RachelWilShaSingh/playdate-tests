#include "pan_system.h"
#include "Structures.h"

#include <stdio.h>
#include <stdlib.h>

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include "sdl_system.h"
#endif

void Pan_Setup( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  sys->fatalError = false;
  sys->wantToQuit = false;

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_Setup( sys );
#endif
}

void Pan_Teardown( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_Teardown( sys );
#endif
}

void Pan_ClearScreen( struct SYSTEM* sys )
{
  // ifdef playdate...
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  pdptr->graphics->clear( kColorWhite );
#elif defined(TARGET_SDL)
  SDL_ClearScreen( sys );
#endif
}

void Pan_DrawScreen( struct SYSTEM* sys )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_DrawScreen( sys );
#endif
}

void Pan_HandleInput( struct SYSTEM* sys )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_HandleInput( sys );
#endif
}

void Pan_Update( struct SYSTEM* sys )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
    SDL_Update( sys );
#endif
}

void Pan_DrawText( struct SYSTEM* sys, struct Font* font, const char* text )
{
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  pdptr->graphics->setFont( font->font );
  pdptr->graphics->drawText( text, strlen( text ), kASCIIEncoding, 10, 10 );
#elif defined(TARGET_SDL)
  // TODO: Allow different text colors
  SDL_Color textColor = { 0, 0, 0 };
  SDL_Surface* textSurface = NULL;
  textSurface = TTF_RenderText_Solid( font->font, text, textColor );
  SDL_BlitSurface( textSurface, NULL, sys->sdl_surface, NULL );
  // TODO: This should be handled better
  SDL_FreeSurface( textSurface );
#endif
}

struct Font Pan_TryLoadFont( const char* path )
{
  struct Font font;

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  const char* err;
  font.font = pdptr->graphics->loadFont( path, &err );
  if ( font.font == NULL )
    {
      pdptr->system->error( "%s:%i loadFont %s: %s", __FILE__, __LINE__, path, err );
    }
  //printf( "[TryLoadFont] LCDFont address is: %p \n", (void*)font.font );
#elif defined(TARGET_SDL)
  // TODO: Need to specify size
  font.font = TTF_OpenFont( path, 28 );
  if ( font.font == NULL )
    {
      printf( "%s:%i ERROR: TTF_OpenFont:  %s: %s", __FILE__, __LINE__, path, TTF_GetError() );
    }
#endif
  return font;
}
