#ifndef _STRUCTURES
#define _STRUCTURES

#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#include "pd_system.h"
#elif defined(TARGET_SDL)
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#endif

#include <stdbool.h>

enum InputState {
  INACTIVE = 0, BEGIN_PUSH = 1, HELD_DOWN = 2, BEGIN_RELEASE = 3
};

struct Input {
  enum InputState buttonLeft;
  enum InputState buttonRight;
  enum InputState buttonUp;
  enum InputState buttonDown;
  enum InputState buttonActionL;
  enum InputState buttonActionR;
};

struct SYSTEM {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
#elif defined(TARGET_SDL)
  SDL_Window* sdl_window;
  SDL_Surface* sdl_surface;
#endif
  bool fatalError;
  bool wantToQuit;
  struct Input input;
};

struct Font {
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  LCDFont* font;
#elif defined(TARGET_SDL)
  TTF_Font* font;
#endif
};


#endif
