// https://gitlab.com/RachelWilShaSingh/playdate-tests
// Rachel Singh

#include <stdio.h>
#include <stdlib.h>

#include "Structures.h"
#include "Functions.h"
#include "pan_system.h"

struct Font testFont;

void Setup( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_Setup( sys );

  // Different font depending on version so need this nonagnostic version unfortunately.
#if defined(TARGET_SIMULATOR) || defined(TARGET_PLAYDATE)
  testFont = Pan_TryLoadFont( "/System/Fonts/Asheville-Sans-14-Bold.pft" );
  printf( "[Setup] LCDFont address is: %p \n", (void*)testFont.font );
#elif defined(TARGET_SDL)
  testFont = Pan_TryLoadFont( "Moosomnia.ttf" );
#endif
}

void Teardown( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_Teardown( sys );
}

void HandleInput( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_HandleInput( sys );
}

void Update( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
    Pan_Update( sys );
}

void Draw( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  Pan_ClearScreen( sys );

  printf( "KEY STATES! INACTIVE = 0, BEGIN_PUSH = 1, HELD_DOWN = 2, BEGIN_RELEASE = 3 \n" );
  printf( "sys->input.buttonLeft:    %d \n", sys->input.buttonLeft );
  printf( "sys->input.buttonRight:   %d \n", sys->input.buttonRight );
  printf( "sys->input.buttonUp:      %d \n", sys->input.buttonUp );
  printf( "sys->input.buttonDown:    %d \n", sys->input.buttonDown );
  printf( "sys->input.buttonActionL: %d \n", sys->input.buttonActionL );
  printf( "sys->input.buttonActionR: %d \n", sys->input.buttonActionR );
  printf( "\n" );

  if      ( sys->input.buttonLeft    == BEGIN_PUSH || sys->input.buttonLeft    == HELD_DOWN ) { Pan_DrawText( sys, &testFont, "Left" );  }
  else if ( sys->input.buttonRight   == BEGIN_PUSH || sys->input.buttonRight   == HELD_DOWN ) { Pan_DrawText( sys, &testFont, "Right" ); }
  else if ( sys->input.buttonUp      == BEGIN_PUSH || sys->input.buttonUp      == HELD_DOWN ) { Pan_DrawText( sys, &testFont, "Up" );    }
  else if ( sys->input.buttonDown    == BEGIN_PUSH || sys->input.buttonDown    == HELD_DOWN ) { Pan_DrawText( sys, &testFont, "Down" );  }
  else if ( sys->input.buttonActionL == BEGIN_PUSH || sys->input.buttonActionL == HELD_DOWN ) { Pan_DrawText( sys, &testFont, "Left Action Button" ); }
  else if ( sys->input.buttonActionR == BEGIN_PUSH || sys->input.buttonActionR == HELD_DOWN ) { Pan_DrawText( sys, &testFont, "Right Action Button" ); }


  Pan_DrawScreen( sys );
}
