#include "sdl_system.h"
#include "Structures.h"

void SDL_Setup( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );

  sys->sdl_window = NULL;
  sys->sdl_surface = NULL;

  if ( SDL_Init( SDL_INIT_VIDEO ) < 0 )
    {
      printf( "%s:%i ERROR: SDL_Init: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_Init successful \n", __FILE__, __LINE__ ); }

  if ( TTF_Init() == -1 )
    {
      printf( "%s:%i ERROR: TTF_Init: %s \n", __FILE__, __LINE__, TTF_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i TTF_Init successful \n", __FILE__, __LINE__ ); }

  sys->sdl_window = SDL_CreateWindow( "Test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 400, 240, SDL_WINDOW_SHOWN );
  if ( sys->sdl_window == NULL )
    {
      printf( "%s:%i ERROR: SDL_CreateWindow: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_CreateWindow successful \n", __FILE__, __LINE__ ); }

  sys->sdl_surface = SDL_GetWindowSurface( sys->sdl_window );
  if ( sys->sdl_surface == NULL )
    {
      printf( "%s:%i ERROR: SDL_GetWindowSurface: %s \n", __FILE__, __LINE__, SDL_GetError() );
      sys->fatalError = true;
    }
  else { printf( "%s:%i SDL_GetWindowSurface successful \n", __FILE__, __LINE__ ); }

  sys->input.buttonLeft    = INACTIVE;
  sys->input.buttonRight   = INACTIVE;
  sys->input.buttonUp      = INACTIVE;
  sys->input.buttonDown    = INACTIVE;
  sys->input.buttonActionL = INACTIVE;
  sys->input.buttonActionR = INACTIVE;
}

void SDL_Teardown( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_DestroyWindow( sys->sdl_window );
  TTF_Quit();
  SDL_Quit();
}

void SDL_ClearScreen( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_FillRect( sys->sdl_surface, NULL, SDL_MapRGB( sys->sdl_surface->format, 0xFF, 0x00, 0xFF ) );
}

void SDL_DrawScreen( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_UpdateWindowSurface( sys->sdl_window );
}

void SDL_Update( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );

    if ( sys->input.buttonUp       == BEGIN_RELEASE ) { sys->input.buttonUp      = INACTIVE; }
    if ( sys->input.buttonDown     == BEGIN_RELEASE ) { sys->input.buttonDown    = INACTIVE; }
    if ( sys->input.buttonLeft     == BEGIN_RELEASE ) { sys->input.buttonLeft    = INACTIVE; }
    if ( sys->input.buttonRight    == BEGIN_RELEASE ) { sys->input.buttonRight   = INACTIVE; }
    if ( sys->input.buttonActionL  == BEGIN_RELEASE ) { sys->input.buttonActionL = INACTIVE; }
    if ( sys->input.buttonActionR  == BEGIN_RELEASE ) { sys->input.buttonActionR = INACTIVE; }
}

void SDL_HandleInput( struct SYSTEM* sys )
{
//  printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  SDL_Event e;
  while ( SDL_PollEvent( &e ) )
    {
      if ( e.type == SDL_QUIT ) {
        sys->wantToQuit = true;
      }
      else if ( e.type == SDL_KEYDOWN )
      {
        // INACTIVE -> BEGIN_PUSH -> HELD_DOWN
        if      ( e.key.keysym.sym == SDLK_w && sys->input.buttonUp      == INACTIVE   ) { sys->input.buttonUp    = BEGIN_PUSH;   }
        else if ( e.key.keysym.sym == SDLK_w && sys->input.buttonUp      == BEGIN_PUSH ) { sys->input.buttonUp    = HELD_DOWN;    }

        if      ( e.key.keysym.sym == SDLK_s && sys->input.buttonDown    == INACTIVE   ) { sys->input.buttonDown  = BEGIN_PUSH;   }
        else if ( e.key.keysym.sym == SDLK_s && sys->input.buttonDown    == BEGIN_PUSH ) { sys->input.buttonDown  = HELD_DOWN;    }

        if      ( e.key.keysym.sym == SDLK_a && sys->input.buttonLeft    == INACTIVE   ) { sys->input.buttonLeft  = BEGIN_PUSH;   }
        else if ( e.key.keysym.sym == SDLK_a && sys->input.buttonLeft    == BEGIN_PUSH ) { sys->input.buttonLeft  = HELD_DOWN;    }

        if      ( e.key.keysym.sym == SDLK_d && sys->input.buttonRight   == INACTIVE   ) { sys->input.buttonRight = BEGIN_PUSH;   }
        else if ( e.key.keysym.sym == SDLK_d && sys->input.buttonRight   == BEGIN_PUSH ) { sys->input.buttonRight = HELD_DOWN;    }

        if      ( e.key.keysym.sym == SDLK_j && sys->input.buttonActionL == INACTIVE   ) { sys->input.buttonActionL = BEGIN_PUSH; }
        else if ( e.key.keysym.sym == SDLK_j && sys->input.buttonActionL == BEGIN_PUSH ) { sys->input.buttonActionL = HELD_DOWN;  }

        if      ( e.key.keysym.sym == SDLK_k && sys->input.buttonActionR == INACTIVE   ) { sys->input.buttonActionR = BEGIN_PUSH; }
        else if ( e.key.keysym.sym == SDLK_k && sys->input.buttonActionR == BEGIN_PUSH ) { sys->input.buttonActionR = HELD_DOWN;  }
      }
      else if ( e.type == SDL_KEYUP )
      {
        // HELD_DOWN -> BEGIN_RELEASE -> INACTIVE
        if      ( e.key.keysym.sym == SDLK_w && ( sys->input.buttonUp      == HELD_DOWN || sys->input.buttonUp      == BEGIN_PUSH )     ) { sys->input.buttonUp      = BEGIN_RELEASE; }
        if      ( e.key.keysym.sym == SDLK_s && ( sys->input.buttonDown    == HELD_DOWN || sys->input.buttonDown    == BEGIN_PUSH )     ) { sys->input.buttonDown    = BEGIN_RELEASE; }
        if      ( e.key.keysym.sym == SDLK_a && ( sys->input.buttonLeft    == HELD_DOWN || sys->input.buttonLeft    == BEGIN_PUSH )     ) { sys->input.buttonLeft    = BEGIN_RELEASE; }
        if      ( e.key.keysym.sym == SDLK_d && ( sys->input.buttonRight   == HELD_DOWN || sys->input.buttonRight   == BEGIN_PUSH )     ) { sys->input.buttonRight   = BEGIN_RELEASE; }
        if      ( e.key.keysym.sym == SDLK_j && ( sys->input.buttonActionL == HELD_DOWN || sys->input.buttonActionL == BEGIN_PUSH )     ) { sys->input.buttonActionL = BEGIN_RELEASE; }
        if      ( e.key.keysym.sym == SDLK_k && ( sys->input.buttonActionR == HELD_DOWN || sys->input.buttonActionR == BEGIN_PUSH )     ) { sys->input.buttonActionR = BEGIN_RELEASE; }
      }
    }
}
