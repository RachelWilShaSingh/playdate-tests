#include "pd_system.h"
#include "pd_api.h"
#include "Functions.h"

#include <stdio.h>
#include <stdlib.h>

PlaydateAPI* pdptr = NULL;

int eventHandler( PlaydateAPI* playdate, PDSystemEvent event, uint32_t arg )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  struct SYSTEM unusedSystem;
  if ( event == kEventInit )
    {
      Playdate_Setup( playdate );
      Setup( &unusedSystem );
    }
  else if ( event == kEventTerminate )
    {
      Playdate_Teardown( playdate );
      Teardown( &unusedSystem );
    }
}

void Playdate_Setup( PlaydateAPI* playdate )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  pdptr = playdate;
  // Set a callback - the Update function gets called each cycle
  pdptr->system->setUpdateCallback( Playdate_Update, playdate );

#ifdef TARGET_PLAYDATE
  printf( "\n TARGET_PLAYDATE \n" );
#endif

#ifdef TARGET_SIMULATOR
  printf( "\n SIMULATOR \n" );
#endif
}

void Playdate_Teardown( PlaydateAPI* playdate )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
}

static int Playdate_Update( void* pdvoid )
{
  //printf( "FUNCTION: %s (%s) \n", __func__, __FILE__ );
  PlaydateAPI* playdate = pdvoid;
  struct SYSTEM system;

  // Read inputs
  PDButtons current, pushed, released;
  playdate->system->getButtonState( &current, &pushed, &released );

  // TODO: Do this better...
  if      ( pushed & kButtonLeft   )  { system.input.buttonLeft = BEGIN_PUSH; }
  else if ( current & kButtonLeft  )  { system.input.buttonLeft = HELD_DOWN; }
  else if ( released & kButtonLeft )  { system.input.buttonLeft = BEGIN_RELEASE; }
  else                                { system.input.buttonLeft = INACTIVE; }

  if      ( pushed & kButtonRight   ) { system.input.buttonRight = BEGIN_PUSH; }
  else if ( current & kButtonRight  ) { system.input.buttonRight = HELD_DOWN; }
  else if ( released & kButtonRight ) { system.input.buttonRight = BEGIN_RELEASE; }
  else                                { system.input.buttonRight = INACTIVE; }

  if      ( pushed & kButtonUp   )    { system.input.buttonUp = BEGIN_PUSH; }
  else if ( current & kButtonUp  )    { system.input.buttonUp = HELD_DOWN; }
  else if ( released & kButtonUp )    { system.input.buttonUp = BEGIN_RELEASE; }
  else                                { system.input.buttonUp = INACTIVE; }

  if      ( pushed & kButtonDown   ) { system.input.buttonDown = BEGIN_PUSH; }
  else if ( current & kButtonDown  ) { system.input.buttonDown = HELD_DOWN; }
  else if ( released & kButtonDown ) { system.input.buttonDown = BEGIN_RELEASE; }
  else                               { system.input.buttonDown = INACTIVE; }

  if      ( pushed & kButtonB   )    { system.input.buttonActionL = BEGIN_PUSH; }
  else if ( current & kButtonB  )    { system.input.buttonActionL = HELD_DOWN; }
  else if ( released & kButtonB )    { system.input.buttonActionL = BEGIN_RELEASE; }
  else                               { system.input.buttonActionL = INACTIVE; }

  if      ( pushed & kButtonA   )    { system.input.buttonActionR = BEGIN_PUSH; }
  else if ( current & kButtonA  )    { system.input.buttonActionR = HELD_DOWN; }
  else if ( released & kButtonA )    { system.input.buttonActionR = BEGIN_RELEASE; }
  else                               { system.input.buttonActionR = INACTIVE; }


  HandleInput( &system );
  Update( &system );
  Draw( &system );
  return 1;
}

