// Bitmap table
// https://gitlab.com/RachelWilShaSingh/playdate-tests
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include "pd_api.h"

enum Direction {
  SOUTH = 0,
  EAST = 1,
  NORTH = 2,
  WEST = 3
};

struct Rectangle {
  float x;
  float y;
  int width;
  int height;
};

// -- STRUCT-O-GAMEDATA ------------------------------------------------------//
struct {
  PlaydateAPI* playdate;
  const char* err;
  int SCREEN_WIDTH;
  int SCREEN_HEIGHT;
  int GRID_WIDTH;

  struct {
    LCDBitmapTable* bmptPlayer;
    LCDBitmap* bmpGround;
    LCDBitmap* bmpStick;
  } GRAPHICS;

  struct {
    SamplePlayer* SAMPLEPLAYER;
    AudioSample* collect;
  } AUDIO;

  struct {
    int score;
    enum Direction direction;
    struct Rectangle pos;
    float speed;
    float frame;
    float frameMax;
  } PLAYER;

  struct {
    struct Rectangle pos;
  } STICK;

  LCDFont* font;
} GAMEDATA;

struct Test
{
  int a;
};

// -- FUNCTION DECLARATIONS --------------------------------------------------//
void SetupGame( PlaydateAPI* playdate );
void TeardownGame( PlaydateAPI* playdate );
void TryLoadBitmapTable( LCDBitmapTable** table, const char* path );
void TryLoadBitmap( LCDBitmap** bitmap, const char* path );
void TryLoadFont( LCDFont** font, const char* path );
void TryLoadAudioSample( AudioSample** sound, const char* path );
void LoadAssets();
void SetupEntities();
static int Update( void* pdvoid );
void UpdateTimers();
void HandleInput();
void CollectStick();
void Draw();
void SetRandomPosition( float* x, float* y, int maxWidth, int maxHeight );
float GetDistance( struct Rectangle rect1, struct Rectangle rect2 );
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function


// -- FUNCTION DEFINITIONS ---------------------------------------------------//
void TryLoadBitmap( LCDBitmap** bitmap, const char* path )
{
  //printf( " # Address of bitmap arg is                 %p \n", (void*)bitmap );
  //printf( " # Address of GAMEDATA.GRAPHICS.bmpStick is %p \n", (void*)&GAMEDATA.GRAPHICS.bmpStick );
  printf( " \n * Try to load bitmap: %s", path );
  *bitmap = GAMEDATA.playdate->graphics->loadBitmap( path, &GAMEDATA.err );
  if ( bitmap == NULL )
    {
      GAMEDATA.playdate->system->error( "%s:%i Couldn't load bitmap %s: %s", __FILE__, __LINE__, path, GAMEDATA.err );
      *bitmap = GAMEDATA.playdate->graphics->loadBitmap( "images/default.png", &GAMEDATA.err );
    }
}

void TryLoadFont( LCDFont** font, const char* path )
{
  printf( " * Try to load font: %s \n", path );
  *font = GAMEDATA.playdate->graphics->loadFont( path, &GAMEDATA.err );
  if ( font == NULL )
    {
      GAMEDATA.playdate->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, path, GAMEDATA.err );
    }
}

void TryLoadBitmapTable( LCDBitmapTable** table, const char* path )
{
  printf( " * Try to load bitmap table: %s \n", path );
  *table = GAMEDATA.playdate->graphics->loadBitmapTable( path, &GAMEDATA.err );
  if ( table == NULL )
    {
      GAMEDATA.playdate->system->error( "%s:%i Couldn't load bitmap table %s: %s", __FILE__, __LINE__, path, GAMEDATA.err );
    }
}

void TryLoadAudioSample( AudioSample** sound, const char* path )
{
  printf( " * Try to load audio sample: %s \n", path );
  *sound = GAMEDATA.playdate->sound->sample->load( path );
  if ( sound == NULL )
    {
      GAMEDATA.playdate->system->error( "%s:%i Couldn't load audio sample %s", __FILE__, __LINE__, path );
    }
}

int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
    {
      SetupGame( pd );
    }
  else if ( event == kEventTerminate )
    {
      TeardownGame( pd );
    }
}

void SetupGame( PlaydateAPI* playdate )
{
  printf( "\n ** PICKIN' STICKS ** \n" );
  srand( time( NULL ) );
  GAMEDATA.playdate = playdate;
  GAMEDATA.SCREEN_WIDTH = 400;
  GAMEDATA.SCREEN_HEIGHT = 240;
  GAMEDATA.GRID_WIDTH = 20;

  playdate->system->setUpdateCallback( Update, playdate );
  GAMEDATA.AUDIO.SAMPLEPLAYER = GAMEDATA.playdate->sound->sampleplayer->newPlayer();

  LoadAssets();
  SetupEntities();

  printf( "\n\n Done with setup 0:) \n" );
}

void LoadAssets()
{
  TryLoadFont( &GAMEDATA.font, "/System/Fonts/Asheville-Sans-14-Bold.pft" );
  TryLoadBitmap( &GAMEDATA.GRAPHICS.bmpStick, "images/stick.png" );
  TryLoadBitmap( &GAMEDATA.GRAPHICS.bmpGround, "images/ground.png" );
  TryLoadBitmapTable( &GAMEDATA.GRAPHICS.bmptPlayer, "images/girl" );
  TryLoadAudioSample( &GAMEDATA.AUDIO.collect, "audio/sound1.wav" );
}

void SetupEntities()
{
  GAMEDATA.PLAYER.score = 0;
  GAMEDATA.PLAYER.pos.x = GAMEDATA.SCREEN_WIDTH/2 - 10;
  GAMEDATA.PLAYER.pos.y = GAMEDATA.SCREEN_HEIGHT/2 - 10;
  GAMEDATA.PLAYER.pos.width = 20;
  GAMEDATA.PLAYER.pos.height = 20;
  GAMEDATA.PLAYER.speed = 2;
  GAMEDATA.PLAYER.frame = 0;
  GAMEDATA.PLAYER.frameMax = 2;
  GAMEDATA.PLAYER.direction = SOUTH;

  GAMEDATA.STICK.pos.width = 20;
  GAMEDATA.STICK.pos.height = 20;
  SetRandomPosition( &GAMEDATA.STICK.pos.x, &GAMEDATA.STICK.pos.y, GAMEDATA.SCREEN_WIDTH - GAMEDATA.STICK.pos.width, GAMEDATA.SCREEN_HEIGHT - GAMEDATA.STICK.pos.height );
}

void TeardownGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Teardown game ** \n\n" );
  GAMEDATA.playdate->sound->sampleplayer->freePlayer( GAMEDATA.AUDIO.SAMPLEPLAYER );
  playdate->graphics->freeBitmapTable( GAMEDATA.GRAPHICS.bmptPlayer );
  playdate->graphics->freeBitmap( GAMEDATA.GRAPHICS.bmpStick );
  playdate->graphics->freeBitmap( GAMEDATA.GRAPHICS.bmpGround );
}

float GetDistance( struct Rectangle rect1, struct Rectangle rect2 )
{
  rect1.x += rect1.width/2;
  rect1.y += rect1.height/2;
  rect2.x += rect2.width/2;
  rect2.y += rect2.height/2;

  float xDiff = rect2.x - rect1.x;
  float yDiff = rect2.y - rect1.y;
  return sqrt( xDiff * xDiff + yDiff * yDiff );
}

void CollectStick()
{
  // You should have an error check in case the audio didn't load.
  GAMEDATA.playdate->sound->sampleplayer->setSample( GAMEDATA.AUDIO.SAMPLEPLAYER, GAMEDATA.AUDIO.collect );
  GAMEDATA.playdate->sound->sampleplayer->play( GAMEDATA.AUDIO.SAMPLEPLAYER, 1, 1.0 ); // Returns 1 on success (always)

  GAMEDATA.PLAYER.score += 1;

  SetRandomPosition( &GAMEDATA.STICK.pos.x, &GAMEDATA.STICK.pos.y, GAMEDATA.SCREEN_WIDTH - GAMEDATA.STICK.pos.width, GAMEDATA.SCREEN_HEIGHT - GAMEDATA.STICK.pos.height );
}

void UpdateTimers()
{
  GAMEDATA.PLAYER.frame += 0.1;
  if ( GAMEDATA.PLAYER.frame >= GAMEDATA.PLAYER.frameMax )
    {
      GAMEDATA.PLAYER.frame = 0;
    }
}

static int Update( void* pdvoid )
{
  // PlaydateAPI* playdate = pdvoid;
  UpdateTimers();
  HandleInput();

  // Check for collision
  if ( GetDistance( GAMEDATA.PLAYER.pos, GAMEDATA.STICK.pos ) < GAMEDATA.PLAYER.pos.width )
    {
      CollectStick();
    }

  Draw();

  return 1;
}

void HandleInput()
{
  PDButtons current;
  PDButtons pushed;
  PDButtons released;
  GAMEDATA.playdate->system->getButtonState( &current, &pushed, &released );

  if ( current & kButtonLeft )
    {
      GAMEDATA.PLAYER.direction = WEST;
      GAMEDATA.PLAYER.pos.x -= GAMEDATA.PLAYER.speed;
    }
  else if ( current & kButtonRight )
    {
      GAMEDATA.PLAYER.direction = EAST;
      GAMEDATA.PLAYER.pos.x += GAMEDATA.PLAYER.speed;
    }

  if ( current & kButtonUp )
    {
      GAMEDATA.PLAYER.direction = NORTH;
      GAMEDATA.PLAYER.pos.y -= GAMEDATA.PLAYER.speed;
    }
  else if ( current & kButtonDown )
    {
      GAMEDATA.PLAYER.direction = SOUTH;
      GAMEDATA.PLAYER.pos.y += GAMEDATA.PLAYER.speed;
    }

  // Stay on screen
  if      ( GAMEDATA.PLAYER.pos.x < 0 )                                                  { GAMEDATA.PLAYER.pos.x = 0; }
  else if ( GAMEDATA.PLAYER.pos.x > GAMEDATA.SCREEN_WIDTH - GAMEDATA.PLAYER.pos.width)   { GAMEDATA.PLAYER.pos.x = GAMEDATA.SCREEN_WIDTH - GAMEDATA.PLAYER.pos.width; }
  if      ( GAMEDATA.PLAYER.pos.y < 0 )                                                  { GAMEDATA.PLAYER.pos.y = 0; }
  else if ( GAMEDATA.PLAYER.pos.y > GAMEDATA.SCREEN_HEIGHT - GAMEDATA.PLAYER.pos.height) { GAMEDATA.PLAYER.pos.y = GAMEDATA.SCREEN_HEIGHT - GAMEDATA.PLAYER.pos.height; }
}

void SetRandomPosition( float* x, float* y, int maxWidth, int maxHeight )
{
  *x = rand() % maxWidth;
  *y = rand() % maxHeight;
}

void Draw()
{
  GAMEDATA.playdate->graphics->clear( kColorWhite );

  // Draw grass
  for ( int y = 0; y < GAMEDATA.SCREEN_HEIGHT / GAMEDATA.GRID_WIDTH; y++ )
    {
      for ( int x = 0; x < GAMEDATA.SCREEN_WIDTH / GAMEDATA.GRID_WIDTH; x++ )
        {
          GAMEDATA.playdate->graphics->drawBitmap( GAMEDATA.GRAPHICS.bmpGround, x*GAMEDATA.GRID_WIDTH, y*GAMEDATA.GRID_WIDTH, 0 );
        }
    }

  // Draw stick
  GAMEDATA.playdate->graphics->drawBitmap( GAMEDATA.GRAPHICS.bmpStick, GAMEDATA.STICK.pos.x, GAMEDATA.STICK.pos.y, 0 );

  // Draw player
  LCDBitmap* currentFrame = GAMEDATA.playdate->graphics->getTableBitmap( GAMEDATA.GRAPHICS.bmptPlayer, (int)(GAMEDATA.PLAYER.direction*2 + (int)GAMEDATA.PLAYER.frame) );
  GAMEDATA.playdate->graphics->drawBitmap( currentFrame, GAMEDATA.PLAYER.pos.x, GAMEDATA.PLAYER.pos.y, 0 );


  char txtScore[50];
  sprintf( txtScore, "Score: %d", GAMEDATA.PLAYER.score );
  GAMEDATA.playdate->graphics->setFont( GAMEDATA.font );
  GAMEDATA.playdate->graphics->drawText( txtScore, strlen(txtScore), kASCIIEncoding, 10, 10 );

  GAMEDATA.playdate->system->drawFPS( 400-20,0 );
}
