// Bitmap table
// https://gitlab.com/RachelWilShaSingh/playdate-tests
#include <stdio.h>
#include <stdlib.h>

#include "pd_api.h"

// -- STRUCT-O-GAMEDATA ------------------------------------------------------//
struct {
  LCDFont* font;
  int SCREEN_WIDTH;
  int SCREEN_HEIGHT;
  float frame;
  float frameMax;
  LCDBitmapTable* bitmapTable;
  const char* err;
} GAMEDATA;


// -- FUNCTION DECLARATIONS --------------------------------------------------//
void SetupGame( PlaydateAPI* playdate );
void TeardownGame( PlaydateAPI* playdate );
static int Update( void* pdvoid );
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function


// -- FUNCTION DEFINITIONS ---------------------------------------------------//
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
    {
      SetupGame( pd );
    }
  else if ( event == kEventTerminate )
    {
      TeardownGame( pd );
    }
}

void SetupGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Example ~ Bitmap table test ~ RWSS ** \n\n" );
  GAMEDATA.SCREEN_WIDTH = 400;
  GAMEDATA.SCREEN_HEIGHT = 240;

  // Set a callback - the Update function gets called each cycle
  playdate->system->setUpdateCallback( Update, playdate );

  // Load a font file
  const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";

  // LCDFont* playdate->graphics->loadFont(const char* path, const char** outErr);
  GAMEDATA.font = playdate->graphics->loadFont( fontpath, &GAMEDATA.err );
  if ( GAMEDATA.font == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, GAMEDATA.err );
    }

  // Load a bitmap table
  const char* bitmappath = "robot"; // "robot-table-20-20.png";

  // LCDBitmapTable* playdate->graphics->loadBitmapTable(const char* path, const char** outerr);
  GAMEDATA.bitmapTable = playdate->graphics->loadBitmapTable( bitmappath, GAMEDATA.err );

  GAMEDATA.frame = 0;
  GAMEDATA.frameMax = 2;
}

void TeardownGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Teardown game ** \n\n" );
  // void playdate->graphics->freeBitmapTable(LCDBitmapTable* table);
  playdate->graphics->freeBitmapTable( GAMEDATA.bitmapTable );

}

static int Update( void* pdvoid )
{
  PlaydateAPI* playdate = pdvoid;


  playdate->graphics->clear( kColorWhite );
  playdate->system->drawFPS( 0,0 );

  GAMEDATA.frame += 0.1;
  if ( GAMEDATA.frame >= GAMEDATA.frameMax )
    {
      GAMEDATA.frame = 0;
    }

  LCDBitmap* currentFrame = playdate->graphics->getTableBitmap( GAMEDATA.bitmapTable, (int)(GAMEDATA.frame) );
  playdate->graphics->drawBitmap( currentFrame, 50, 50, 0 );


  char txtFrame[50];
  sprintf( txtFrame, "Frame: %f", GAMEDATA.frame );
  playdate->graphics->setFont( GAMEDATA.font );
  playdate->graphics->drawText( txtFrame, strlen(txtFrame), kASCIIEncoding, 100, 100 );


  return 1;
}
