// Basic Playdate test - display image
// https://gitlab.com/RachelWilShaSingh/playdate-tests
// Rachel Singh

/* *** NOTE: MAKE SURE TO PUT YOUR IMAGE IN THE PATH OF THE .so FILE, SUCH AS build/ *** */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pd_api.h"

struct {
  LCDBitmap* loadedImage;
  LCDBitmap* createdImage;
  const char* err;
} GAMEDATA;

void InitializeGame( PlaydateAPI* pd );
int Update( void* userdata );   
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function

int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
  {
    InitializeGame( pd );
  }
}

void InitializeGame( PlaydateAPI* pd )
{
  printf( "Example ~ Drawing an image to the screen ~ RWSS \n" );
  
  // Set a callback - the Update function gets called each cycle
  pd->system->setUpdateCallback( Update, pd );
  
  // Load image
  GAMEDATA.loadedImage = NULL;
  const char* imagepath = "moosadee-logo";
  GAMEDATA.loadedImage = pd->graphics->loadBitmap( imagepath, &GAMEDATA.err );
  if ( GAMEDATA.loadedImage == NULL )
  {
    pd->system->error( "%s:%i Couldn't load image %s: %s", __FILE__, __LINE__, imagepath, GAMEDATA.err );
    printf( "\n\n ** ERROR ** %s:%i Couldn't load image %s: %s \n\n", __FILE__, __LINE__, imagepath, GAMEDATA.err );
  }
  
  // Create image
  /* LCDSolidColor - kColorBlack, kColorWhite, kColorClear, kColorXOR */
  GAMEDATA.createdImage = NULL;
  GAMEDATA.createdImage = pd->graphics->newBitmap( 50, 50, kColorBlack );
}

int Update( void* userdata ) // Must return int
{
  PlaydateAPI* pd = userdata;
  
  // Draw
  pd->graphics->clear( kColorWhite );
  pd->system->drawFPS( 0,0 );
  
  // void playdate->graphics->drawBitmap(LCDBitmap* bitmap, int x, int y, LCDBitmapFlip flip);
  if ( GAMEDATA.loadedImage == NULL )
  {
    pd->graphics->drawText( GAMEDATA.err, strlen( GAMEDATA.err ), kASCIIEncoding, 100, 100 );
  }
  else
  {
    pd->graphics->drawBitmap( GAMEDATA.loadedImage, 50, 50, 0 );
  }
  
  pd->graphics->drawBitmap( GAMEDATA.createdImage, 175, 175, 0 );
  
  return 1;
}


