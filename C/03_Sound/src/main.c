// Play sound file
// https://gitlab.com/RachelWilShaSingh/playdate-tests
#include <stdio.h>
#include <stdlib.h>

#include "pd_api.h"

// -- STRUCT-O-GAMEDATA ------------------------------------------------------//
struct {
  LCDFont* font;
  LCDBitmap* loadedImage;
  AudioSample* sound1;
  AudioSample* sound2;
  SamplePlayer* soundPlayer;
  FilePlayer*   musicPlayer;
  int SCREEN_WIDTH;
  int SCREEN_HEIGHT;
  const char* err;
} GAMEDATA;


// -- FUNCTION DECLARATIONS --------------------------------------------------//
void SetupGame( PlaydateAPI* playdate );
void TeardownGame( PlaydateAPI* playdate );
static int Update( void* pdvoid );
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function


// -- FUNCTION DEFINITIONS ---------------------------------------------------//
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
    {
      SetupGame( pd );
    }
  else if ( event == kEventTerminate )
    {
      TeardownGame( pd );
    }
}

void SetupGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Example ~ Sound test ~ RWSS ** \n\n" );
  GAMEDATA.SCREEN_WIDTH = 400;
  GAMEDATA.SCREEN_HEIGHT = 240;

  // Set a callback - the Update function gets called each cycle
  playdate->system->setUpdateCallback( Update, playdate );

  // Load a font file
  const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";

  // LCDFont* playdate->graphics->loadFont(const char* path, const char** outErr);
  GAMEDATA.font = playdate->graphics->loadFont( fontpath, &GAMEDATA.err );
  if ( GAMEDATA.font == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, GAMEDATA.err );
    }

  // Load an image
  GAMEDATA.loadedImage = NULL;
  const char* imagepath = "moosadee-logo";

  // LCDBitmap* playdate->graphics->loadBitmap(const char* path, const char** outerr);
  GAMEDATA.loadedImage = playdate->graphics->loadBitmap( imagepath, &GAMEDATA.err );
  if ( GAMEDATA.loadedImage == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load image %s: %s", __FILE__, __LINE__, imagepath, GAMEDATA.err );
      printf( "\n\n ** ERROR ** %s:%i Couldn't load image %s: %s \n\n", __FILE__, __LINE__, imagepath, GAMEDATA.err );
    }


  // Setup sample player
  GAMEDATA.soundPlayer = NULL;
  GAMEDATA.soundPlayer = playdate->sound->sampleplayer->newPlayer();


  // Load an audio file
  const char* samplepath = "sound1.wav";
  // AudioSample* playdate->sound->sample->load(const char* path)
  GAMEDATA.sound1 = playdate->sound->sample->load( samplepath );
  if ( GAMEDATA.sound1 == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load sound %s", __FILE__, __LINE__, samplepath );
      printf( "\n\n ** ERROR ** %s:%i Couldn't load image %s \n\n", __FILE__, __LINE__, imagepath );
    }

  samplepath = "sound2.wav";
  GAMEDATA.sound2 = playdate->sound->sample->load( samplepath );
  if ( GAMEDATA.sound2 == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load sound %s", __FILE__, __LINE__, samplepath );
      printf( "\n\n ** ERROR ** %s:%i Couldn't load image %s \n\n", __FILE__, __LINE__, imagepath );
    }


  GAMEDATA.musicPlayer = playdate->sound->fileplayer->newPlayer();
  // Load music
  const char* musicpath = "song_dude";
  int result = playdate->sound->fileplayer->loadIntoPlayer( GAMEDATA.musicPlayer, musicpath );
  if ( result == 0 )
    {
      printf( "%s:%i ERROR: fileplayer->loadIntoPlayer: Can't find file: %s \n", __FILE__, __LINE__, musicpath );
    }

  // Play music
  playdate->sound->fileplayer->play( GAMEDATA.musicPlayer, 0 );
}

void TeardownGame( PlaydateAPI* playdate )
{
  playdate->sound->sample->freeSample( GAMEDATA.sound1 );
  playdate->sound->sample->freeSample( GAMEDATA.sound2 );
  playdate->sound->sampleplayer->freePlayer( GAMEDATA.soundPlayer );
  playdate->sound->fileplayer->freePlayer( GAMEDATA.musicPlayer );
}

static int Update( void* pdvoid )
{
  PlaydateAPI* playdate = pdvoid;

  // INPUT
  PDButtons current;
  PDButtons pushed;
  PDButtons released;
  playdate->system->getButtonState( &current, &pushed, &released );
  if      ( pushed & kButtonLeft  ) {  }
  else if ( pushed & kButtonRight ) {  }
  else if ( pushed & kButtonUp    ) {  }
  else if ( pushed & kButtonDown  ) {  }

  else if ( pushed & kButtonB )
    {
      // You should have an error check in case the audio didn't load.
      playdate->sound->sampleplayer->setSample( GAMEDATA.soundPlayer, GAMEDATA.sound1 );
      playdate->sound->sampleplayer->play( GAMEDATA.soundPlayer, 1, 1.0 ); // Returns 1 on success (always)
    }
  else if ( pushed & kButtonA )
    {
      // You should have an error check in case the audio didn't load.
      playdate->sound->sampleplayer->setSample( GAMEDATA.soundPlayer, GAMEDATA.sound2 );
      playdate->sound->sampleplayer->play( GAMEDATA.soundPlayer, 1, 1.0 ); // Returns 1 on success (always)
    }



  playdate->graphics->clear( kColorWhite );
  playdate->system->drawFPS( 0,0 );
  playdate->graphics->setFont( GAMEDATA.font );
  playdate->graphics->drawText( "Press a button for sound", strlen("Press a button for sound"), kASCIIEncoding, 100, 200 );



  if ( GAMEDATA.loadedImage == NULL )
    {
      playdate->graphics->drawText( GAMEDATA.err, strlen( GAMEDATA.err ), kASCIIEncoding, 100, 100 );
    }
  else
    {
      playdate->graphics->drawBitmap( GAMEDATA.loadedImage, GAMEDATA.SCREEN_WIDTH/2-40, 50, 0 );
    }


  return 1;
}
