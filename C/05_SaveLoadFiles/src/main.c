// Save/Load
// https://gitlab.com/RachelWilShaSingh/playdate-tests
#include <stdio.h>
#include <stdlib.h>

#include "pd_api.h"

// -- STRUCT-O-GAMEDATA ------------------------------------------------------//
struct {
  LCDFont* font;
  int SCREEN_WIDTH;
  int SCREEN_HEIGHT;
  char fileText[100];
  const char* err;
} GAMEDATA;


// -- FUNCTION DECLARATIONS --------------------------------------------------//
void SetupGame( PlaydateAPI* playdate );
void TeardownGame( PlaydateAPI* playdate );
static int Update( void* pdvoid );
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function


// -- FUNCTION DEFINITIONS ---------------------------------------------------//
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
    {
      SetupGame( pd );
    }
  else if ( event == kEventTerminate )
    {
      TeardownGame( pd );
    }
}

void SetupGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Example ~ Save/Load file test ~ RWSS ** \n\n" );
  GAMEDATA.SCREEN_WIDTH = 400;
  GAMEDATA.SCREEN_HEIGHT = 240;

  // Set a callback - the Update function gets called each cycle
  playdate->system->setUpdateCallback( Update, playdate );

  // Load a font file
  const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";

  // LCDFont* playdate->graphics->loadFont(const char* path, const char** outErr);
  GAMEDATA.font = playdate->graphics->loadFont( fontpath, &GAMEDATA.err );
  if ( GAMEDATA.font == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, GAMEDATA.err );
    }

  // Load file
  const char* path = "save1.txt";

  // SDFile* playdate->file->open(const char* path, FileOptions mode);
  SDFile* file = playdate->file->open( path, kFileReadData );

  // int playdate->file->read(SDFile* file, void* buf, unsigned int len);
  int result = playdate->file->read( file, GAMEDATA.fileText, 50 );

  playdate->file->close( file );

  printf( "\n Total bytes read: %d \n ", result );
  printf( "\n Read in text: '%s' \n", GAMEDATA.fileText );

  const char* errorText = playdate->file->geterr();
  printf( "\n Error text: %s \n", errorText );
}

void TeardownGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Teardown game ** \n\n" );

  // Save some data
  const char* path = "save1.txt";
  const char* text = "0123456789";
  // SDFile* playdate->file->open(const char* path, FileOptions mode);
  SDFile* file = playdate->file->open( path, kFileWrite );

  int result = playdate->file->write( file, text, strlen( text ) );
  printf( "\n Total bytes written: %d \n ", result );

  const char* errorText = playdate->file->geterr();
  printf( "\n Error text: %s \n", errorText );

  playdate->file->close( file );
}

static int Update( void* pdvoid )
{
  PlaydateAPI* playdate = pdvoid;


  playdate->graphics->clear( kColorWhite );
  playdate->system->drawFPS( 0,0 );
  playdate->graphics->setFont( GAMEDATA.font );
  playdate->graphics->drawText( GAMEDATA.fileText, strlen(GAMEDATA.fileText), kASCIIEncoding, 100, 100 );


  return 1;
}
