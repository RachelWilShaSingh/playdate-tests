// Check crank state
// https://gitlab.com/RachelWilShaSingh/playdate-tests
#include <stdio.h>
#include <stdlib.h>

#include "pd_api.h"

// -- STRUCT-O-GAMEDATA ------------------------------------------------------//
struct {
  LCDFont* font;
  int SCREEN_WIDTH;
  int SCREEN_HEIGHT;
  const char* err;
} GAMEDATA;


// -- FUNCTION DECLARATIONS --------------------------------------------------//
void SetupGame( PlaydateAPI* playdate );
void TeardownGame( PlaydateAPI* playdate );
static int Update( void* pdvoid );
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function


// -- FUNCTION DEFINITIONS ---------------------------------------------------//
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
    {
      SetupGame( pd );
    }
  else if ( event == kEventTerminate )
    {
      TeardownGame( pd );
    }
}

void SetupGame( PlaydateAPI* playdate )
{
  printf( "\n\n ** Example ~ Crank test ~ RWSS ** \n\n" );
  GAMEDATA.SCREEN_WIDTH = 400;
  GAMEDATA.SCREEN_HEIGHT = 240;

  // Set a callback - the Update function gets called each cycle
  playdate->system->setUpdateCallback( Update, playdate );

  // Load a font file
  const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";

  // LCDFont* playdate->graphics->loadFont(const char* path, const char** outErr);
  GAMEDATA.font = playdate->graphics->loadFont( fontpath, &GAMEDATA.err );
  if ( GAMEDATA.font == NULL )
    {
      playdate->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, GAMEDATA.err );
    }


}

void TeardownGame( PlaydateAPI* playdate )
{
}

static int Update( void* pdvoid )
{
  PlaydateAPI* playdate = pdvoid;


  // Check crank state
  float crankAngleDegrees = playdate->system->getCrankAngle();
  float crankAngleRadians = crankAngleDegrees * ( 3.14159 / 180 );
  float crankChangeDegrees = playdate->system->getCrankChange();

  // printf( "\n * Crank angle (deg): %f \t Crank angle (rad): %f \t Crank change (deg): %f", crankAngleDegrees, crankAngleRadians, crankChangeDegrees );

  char cad[50], car[50], ccd[50];
  sprintf( cad, "%s %f", "Crank angle (degrees):", crankAngleDegrees );
  sprintf( car, "%s %f", "Crank angle (radians):", crankAngleRadians );
  sprintf( ccd, "%s %f", "Crank change (degrees):", crankChangeDegrees );

  playdate->graphics->clear( kColorWhite );
  playdate->system->drawFPS( 0,0 );
  playdate->graphics->setFont( GAMEDATA.font );

  playdate->graphics->drawText( cad, strlen(cad), kASCIIEncoding, 10, 10 );
  playdate->graphics->drawText( car, strlen(car), kASCIIEncoding, 10, 50 );
  playdate->graphics->drawText( ccd, strlen(ccd), kASCIIEncoding, 10, 90 );




  return 1;
}
