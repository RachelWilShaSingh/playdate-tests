// Basic Playdate test - draw text to screen
// https://gitlab.com/RachelWilShaSingh/playdate-tests
// Rachel Singh
// This was built referencing the "Hello World" example by Dave Hayden in the SDK example files.

#include <stdio.h>
#include <stdlib.h>

#include "pd_api.h"

struct {
  LCDFont* font;
} GAMEDATA;

void InitializeGame( PlaydateAPI* pd );
static int Update( void* pdvoid );
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function

/* PDSystemEvent:
kEventInit, kEventInitLua, kEventLock, kEventUnlock, kEventPause, kEventResume,
kEventTerminate, kEventKeyPressed, kEventKeyReleased, kEventLowPower
https://sdk.play.date/1.9.0/Inside%20Playdate%20with%20C.html#_game_initialization
*/
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
  {
    InitializeGame( pd );
  }
}

void InitializeGame( PlaydateAPI* pd )
{
  printf( "Example ~ Basic text drawing example ~ RWSS" );

  // Set a callback - the Update function gets called each cycle
  pd->system->setUpdateCallback( Update, pd );

  // Load a font file
  const char* err;
  const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";
  GAMEDATA.font = pd->graphics->loadFont( fontpath, &err );
  if ( GAMEDATA.font == NULL )
  {
    pd->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, err );
  }
}

static int Update( void* pdvoid )
{
  PlaydateAPI* pd = pdvoid;
  pd->graphics->clear( kColorWhite );
  pd->system->drawFPS( 0,0 );
  pd->graphics->setFont( GAMEDATA.font );
  pd->graphics->drawText( "Hello World!", strlen("Hello World!"), kASCIIEncoding, 100, 100 );

  return 1;
}
