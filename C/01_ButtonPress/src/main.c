// Basic Playdate test - detect keypresses
// https://gitlab.com/RachelWilShaSingh/playdate-tests
// Rachel Singh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pd_api.h"

struct {
  LCDFont* font;
  char buttonState[100];
} GAMEDATA;

void InitializeGame( PlaydateAPI* pd );
int Update( void* userdata );   // Must return int
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg ); // playdate function

/* PDSystemEvent:
kEventInit, kEventInitLua, kEventLock, kEventUnlock, kEventPause, kEventResume,
kEventTerminate, kEventKeyPressed, kEventKeyReleased, kEventLowPower
https://sdk.play.date/1.9.0/Inside%20Playdate%20with%20C.html#_game_initialization
*/
int eventHandler( PlaydateAPI* pd, PDSystemEvent event, uint32_t arg )
{
  if ( event == kEventInit )
  {
    InitializeGame( pd );
  }
}

void InitializeGame( PlaydateAPI* pd )
{
  printf( "Example ~ Detecting a keypress and displaying text to the screen ~ RWSS" );

  // Set a callback - the Update function gets called each cycle
  pd->system->setUpdateCallback( Update, pd );

  // Load a font file
  const char* err;
  const char* fontpath = "/System/Fonts/Asheville-Sans-14-Bold.pft";
  GAMEDATA.font = pd->graphics->loadFont( fontpath, &err );
  if ( GAMEDATA.font == NULL )
  {
    pd->system->error( "%s:%i Couldn't load font %s: %s", __FILE__, __LINE__, fontpath, err );
  }

  memset( GAMEDATA.buttonState, '\0', sizeof( GAMEDATA.buttonState ) );
}

int Update( void* userdata )
{
  PlaydateAPI* pd = userdata;

  // Check button states
  // https://sdk.play.date/1.9.0/Inside%20Playdate%20with%20C.html#f-system.getButtonState
  // void playdate->system->getButtonState(PDButtons* current, PDButtons* pushed, PDButtons* released)
  PDButtons current;
  PDButtons pushed;
  PDButtons released;
  pd->system->getButtonState( &current, &pushed, &released );

  // I hate C style strings, ugh.
  if      ( pushed & kButtonLeft  ) { strcpy( GAMEDATA.buttonState, "left push" );   }
  else if ( pushed & kButtonRight ) { strcpy( GAMEDATA.buttonState, "right push" );  }
  else if ( pushed & kButtonUp    ) { strcpy( GAMEDATA.buttonState, "up push" );     }
  else if ( pushed & kButtonDown  ) { strcpy( GAMEDATA.buttonState, "down push" );   }
  else if ( pushed & kButtonB     ) { strcpy( GAMEDATA.buttonState, "b push" );      }
  else if ( pushed & kButtonA     ) { strcpy( GAMEDATA.buttonState, "a push" );      }

  char* text = "Hello, world!";

  // Draw
  pd->graphics->clear( kColorWhite );
  pd->system->drawFPS( 0,0 );
  pd->graphics->setFont( GAMEDATA.font );
  pd->graphics->drawText( text, strlen( text ), kASCIIEncoding, 100, 100 );

  if ( GAMEDATA.buttonState != NULL )
  {
    pd->graphics->drawText( GAMEDATA.buttonState, strlen( GAMEDATA.buttonState ), kASCIIEncoding, 10, 50 );
  }


  return 1;
}
