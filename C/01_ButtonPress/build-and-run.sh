# https://sdk.play.date/1.9.0/Inside%20Playdate%20with%20C.html

compilerPath='/media/rachelwil/RachelsSSD1/LIBRARIES/PlaydateSDK-2.2.0/bin/pdc'
simulatorPath='/media/rachelwil/RachelsSSD1/LIBRARIES/PlaydateSDK-2.2.0/bin/PlaydateSimulator'
sourcePath='./src'
outputPath='./Program.pdx'

# pdc [sourcepath] [outputpath]

echo "Compiler path:  $compilerPath"
echo "Simulator path: $simulatorPath"
echo "Source path:    $sourcePath"
echo "Output path:    $outputPath"
echo ""

#echo "Compile source for simulator..."
#make simulator

echo "Compile source for as .pdx..."
make pdc

#echo "Compile source for device..."
#make bin

echo "Opening simulator..."
$simulatorPath $outputPath
