import 'CoreLibs/graphics.lua'
local gfx = playdate.graphics

textentryScreen = {
  key = "textentry",    -- A string identifier for this state
  nextState = "",       -- Set this when you're ready to go to another state
  
  imgBackground = gfx.image.new( "images/menubg.png" ),
  imgCursor     = gfx.image.new( "images/cursor.png" ),
  
  buttons = {
  },
  
  cursor = {
    x = 100,
    width = 30,
    height = 30,
    positions = { 
      { x = 0, y = 0, select = "A" }
    },
    currentPosition = 1
  },
  
  letters = { "A", "B", "C" },
  
  textString = "TEST",
  
  Init = function( self )
    textentryScreen.nextState = ""
    textentryScreen.CreateButtons()
  end,
  
  Update = function( self )
  end,
  
  Draw = function( self )
    textentryScreen.imgBackground:draw( 0, 0 )
    gfx.drawTextAligned( "TEXT ENTRY SCREEN", 25, 3, kTextAlignment.left )
    
    -- Draw textbox
    gfx.drawRect( 5, 30, 400-10, 20 )
    gfx.drawText( textentryScreen.textString, 7, 32 )
    
    -- Draw cursor
    --textentryScreen.imgCursor:draw( textentryScreen.cursor["x"], textentryScreen.cursor["yPositions"][ textentryScreen.cursor["currentPosition"] ] )
  end,
  
  Cleanup = function( self )
    -- Free space here as needed
  end,
  
  CreateButtons = function( self )
  end,
  
  Handle_upButtonDown = function( self )
    --textentryScreen.cursor["currentPosition"] = textentryScreen.cursor["currentPosition"] - 1
    --if ( textentryScreen.cursor["currentPosition"] == 0 ) then
      --textentryScreen.cursor["currentPosition"] = #textentryScreen.cursor["yPositions"]
    --end
  end,
  
  
  -- Button handlers
  Handle_downButtonDown = function( self )
    --textentryScreen.cursor["currentPosition"] = textentryScreen.cursor["currentPosition"] + 1
    --if ( textentryScreen.cursor["currentPosition"] > #textentryScreen.cursor["yPositions"] ) then
      --textentryScreen.cursor["currentPosition"] = 1
    --end
  end,
  
  Handle_BButtonDown = function( self )
    --cursorPosition = textentryScreen.cursor["currentPosition"]
    --textentryScreen.nextState = textentryScreen.cursor["selections"][ cursorPosition ]
  end,
  
  
  -- Unused
  Handle_upButtonUp       = function( self ) 
  end,
  
  Handle_downButtonUp     = function( self ) 
  end,
  
  Handle_leftButtonDown   = function( self ) 
  end,
  
  Handle_leftButtonUp     = function( self ) 
  end,
  
  Handle_rightButtonDown  = function( self ) 
  end,
  
  Handle_rightButtonUp    = function( self ) 
  end,
  
  Handle_AButtonDown      = function( self ) 
  end,
  
  Handle_AButtonUp        = function( self ) 
  end,
  
  Handle_BButtonUp        = function( self ) 
  end,
}
