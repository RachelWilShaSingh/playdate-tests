import 'CoreLibs/graphics.lua'

print( "Example ~ Bouncy ball ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

minX = 0
maxX = 400
minY = 0
maxY = 240

ballX = maxX / 2
ballY = maxY / 2
ballR = 15
velX = 2
velY = 2

function playdate.update()
  
  -- Updates
  ballX = ballX + velX
  ballY = ballY + velY
  
  if ( ballX > maxX or ballX < minX ) then
    velX = -velX
  end
  
  if ( ballY > maxY or ballY < minY ) then
    velY = -velY
  end
  
  -- Drawing
  gfx.clear( gfx.kColorWhite )
  gfx.setColor( gfx.kColorBlack )
  gfx.fillCircleAtPoint( ballX, ballY, ballR )
  
end
