-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh

import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )
screenSize = { width = 400, height = 240 }

speed = 1

xOffset = 0

background = gfx.image.new( "Backyard.png" )
imageWidth = 1313

function playdate.update()
  gfx.clear( gfx.kColorWhite )
  
  crankAngleDegrees = playdate.getCrankPosition()
  
  crankRatio = crankAngleDegrees / 360
  xOffset = imageWidth * crankRatio
  
  background:draw( xOffset, 0 )  
  background:draw( xOffset - imageWidth, 0 ) 
  
end
