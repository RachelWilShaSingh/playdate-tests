import 'CoreLibs/graphics.lua'
print( "Example ~ MooseInvaders ~ RWSS" )

local gfx = playdate.graphics
local snd = playdate.sound
playdate.display.setRefreshRate( 50 )

------------------------------------------------------------------------ Functions

function GetRandomX() return math.random( 0, screenSize["width"] - screenSize["gridWidth"] ) end
function GetRandomY() return math.random( screenSize["hudHeight"], screenSize["height"] - screenSize["gridWidth"] ) end

function KeepInBounds( obj )
  if ( obj["x"] < 0 ) then obj["x"] = 0 end
  
  if ( obj["x"] > screenSize["width"] - screenSize["gridWidth"] ) then
    obj["x"] = screenSize["width"] - screenSize["gridWidth"]
  end
  
  if ( obj["y"] < screenSize["hudHeight"] ) then obj["y"] = screenSize["hudHeight"] end
  
  if ( obj["y"] > screenSize["height"] - screenSize["gridWidth"] ) then
    obj["y"] = screenSize["height"] - screenSize["gridWidth"]
  end
end

function IsCollision( objA, objB )
  return (  objA["x"]                   <= objB["x"] + objB["width"] and
            objA["x"] + objA["width"]   >= objB["x"] + objB["width"] and
            objA["y"]                   <= objB["y"] + objB["height"] and
            objA["y"] + objA["height"]  >= objB["y"] + objB["height"] )
end

function DrawCharacter( char )
  char["image"]:draw( char["x"], char["y"], gfx.kImageUnflipped, math.floor( char["frame"] ) * char["width"], char["direction"] * char["height"], char["width"], char["height"] )
end

function AnimateCharacter( char )
  char["frame"] = char["frame"] + char["frameSpeed"]
  if ( char["frame"] >= char["frameMax"] ) then
    char["frame"] = 0
  end
end

function UpdateAlien( char )
  char["x"] = char["x"] + char["speed"]
  if ( char["x"] >= screenSize["width"] - char["width"] or char["x"] < 0 ) then
    char["speed"] = -char["speed"]
    char["y"] = char["y"] + char["height"]
  end
end

function HandleMovement( char, direction )
  if      ( direction == "LEFT" ) then
    char["speed"] = -char["speedMax"]
  elseif  ( direction == "RIGHT" ) then
    char["speed"] = char["speedMax"]
  else
    char["speed"] = 0
  end
end

function UpdatePlayer( char )
  char["x"] = char["x"] + char["speed"]
  KeepInBounds( char )
  
  if ( char["bulletCooldown"] > 0 ) then
    char["bulletCooldown"] = char["bulletCooldown"] - 1
  end
end

function UpdateBullet( char )
  char["y"] = char["y"] + char["speed"]
  if ( char["y"] < 0 ) then
    return "DESTROY"
  else
    return ""
  end
end

function ShootBullet( shootSource )
  if ( shootSource["bulletCooldown"] > 0 ) then return end
  bullet = BulletBuilder( shootSource )
  table.insert( bulletObjects, bullet )
  shootSource["bulletCooldown"] = shootSource["bulletCooldownMax"]
  sounds["shoot"]:play()
end

function AlienBuilder( x, y )
  alien = {}
  alien["x"] = x
  alien["y"] = y
  alien["width"] = 20
  alien["height"] = 20
  alien["speed"] = 1
  alien["image"] = images["invader"]
  alien["frame"] = 0
  alien["frameMax"] = 2
  alien["frameSpeed"] = 0.1
  alien["direction"] = 0
  return alien
end

function BulletBuilder( shootSource )
  bullet = {}
  bullet["width"] = 5
  bullet["height"] = 10
  bullet["image"] = images["bullet"]
  bullet["x"] = shootSource["x"] + shootSource["width"] / 2
  bullet["y"] = shootSource["y"]
  bullet["direction"] = 0
  bullet["speed"] = -2
  bullet["frame"] = 0
  bullet["frameMax"] = 1
  bullet["frameSpeed"] = 0.1
  return bullet
end

function DestroyElement( objects, key )
  objects[key] = nil
end

------------------------------------------------------------------------ Game object initialization

screenSize = { width = 400, height = 240, gridWidth = 20, hudHeight = 20 }
gfx.setBackgroundColor( gfx.kColorBlack )

sounds = {
  shoot     = snd.sampleplayer.new( "audio/bulletshoot.wav" ),
  deadAlien = snd.sampleplayer.new( "audio/aliendie.wav" )
}

images = {
  invader = gfx.image.new( "images/invader.png" ),
  ship    = gfx.image.new( "images/ship.png" ),
  bullet  = gfx.image.new( "images/bullet.png" )
}

alienObjects = {}
for y = 0, 1 do
  for x = 0, 5 do
    table.insert( alienObjects, AlienBuilder( x * (screenSize["gridWidth"]+20), y * (screenSize["gridWidth"]+20)+ 25 ) )
  end
end

bulletObjects = {}

playerObject = {}
playerObject["width"] = 20
playerObject["height"] = 20
playerObject["x"] = screenSize["width"] / 2 - playerObject["width"] / 2
playerObject["y"] = screenSize["height"] - playerObject["height"]
playerObject["image"] = gfx.image.new( "images/ship.png" )
playerObject["score"] = 0
playerObject["speed"] = 0
playerObject["speedMax"] = 2
playerObject["direction"] = 0
playerObject["frame"] = 0
playerObject["frameMax"] = 2
playerObject["frameSpeed"] = 0.1
playerObject["bulletCooldown"] = 0
playerObject["bulletCooldownMax"] = 50

------------------------------------------------------------------------ Update function

function playdate.update()    
  gfx.clear( gfx.kColorBlack )
  gfx.setColor( gfx.kColorBlack )
  
  for key, alien in pairs( alienObjects ) do
    AnimateCharacter( alien )
    UpdateAlien( alien )
    DrawCharacter( alien )
    
    -- Check for collision with bullet
    
    for bkey, bullet in pairs( bulletObjects ) do
      if ( IsCollision( alien, bullet ) ) then
        DestroyElement( bulletObjects, bkey )
        DestroyElement( alienObjects, key )
        playerObject["score"] = playerObject["score"] + 10
        sounds["deadAlien"]:play()
      end
    end
  end
  
  for key, bullet in pairs( bulletObjects ) do
    state = UpdateBullet( bullet )
    if ( state == "DESTROY" ) then
      DestroyElement( bulletObjects, key )
    else
      DrawCharacter( bullet )
    end
  end

  UpdatePlayer( playerObject )
  DrawCharacter( playerObject )

  gfx.setColor( gfx.kColorWhite )
  gfx.fillRect( 0, 0, screenSize["width"], screenSize["hudHeight"] )
  gfx.drawText( "Score: " .. playerObject["score"], 2, 2 )
  gfx.drawText( "Moose Invaders", 270, 0 )  
  
end

------------------------------------------------------------------------ Callback functions

function playdate.leftButtonDown()    HandleMovement( playerObject, "LEFT" )              end
function playdate.leftButtonUp()      HandleMovement( playerObject, "" )                  end
function playdate.rightButtonDown()   HandleMovement( playerObject, "RIGHT" )             end
function playdate.rightButtonUp()     HandleMovement( playerObject, "" )                  end
function playdate.BButtonDown()       ShootBullet( playerObject )                         end
