import 'CoreLibs/graphics.lua'

print( "Example ~ Bouncy ball ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

minX = 0
maxX = 400
minY = 0
maxY = 240

ballCount = 100 -- Increase this to 'stress test' the playdate later?

function CreateBall()
  newBall = {}
  newBall["x"] = math.random( minX, maxX )
  newBall["y"] = math.random( minY, maxY )
  newBall["velX"] = math.random( -2, 2 )
  newBall["velY"] = math.random( -2, 2 )
  newBall["radius"] = math.random( 10, 15 )
  return newBall
end

function UpdateBalls( ballList )
  for i = 1, ballCount, 1 do
    ballList[i]["x"] = ballList[i]["x"] + ballList[i]["velX"]
    ballList[i]["y"] = ballList[i]["y"] + ballList[i]["velY"]
    
    if ( ballList[i]["x"] > maxX or ballList[i]["x"] < minX ) then
      ballList[i]["velX"] = -ballList[i]["velX"]
    end
    
    if ( ballList[i]["y"] > maxY or ballList[i]["y"] < minY ) then
      ballList[i]["velY"] = -ballList[i]["velY"]
    end
  end
end

function DrawBalls( ballList )
  gfx.setColor( gfx.kColorXOR )
  for i = 1, ballCount, 1 do
    gfx.fillCircleAtPoint( ballList[i]["x"], ballList[i]["y"], ballList[i]["radius"] )
  end
end

-- Table of balls
local ballList = {}

for i = 1, ballCount, 1 do
  table.insert( ballList, CreateBall() )
end


function playdate.update()
  
  ---- Updates
  UpdateBalls( ballList )
  
  -- Drawing
  gfx.clear( gfx.kColorWhite )
  DrawBalls( ballList )
  
end
