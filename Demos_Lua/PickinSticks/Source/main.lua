import 'CoreLibs/graphics.lua'

print( "Example ~ Pickin' Sticks ~ RWSS" )

local gfx = playdate.graphics
local snd = playdate.sound

playdate.display.setRefreshRate( 50 )

screenSize = { width = 400, height = 240, gridWidth = 20, hudHeight = 20 }

images = {}
images["ground"]  = gfx.image.new( "images/ground.png" )

sheetDirection = {}
sheetDirection["left"] = 0
sheetDirection["up"] = 1
sheetDirection["right"] = 2
sheetDirection["down"] = 3

soundCollect = snd.sampleplayer.new( "audio/sound1.wav" )
soundCollect:setVolume( 0.5 )

gfx.setBackgroundColor( gfx.kColorWhite )

function DrawGround()  
  for y = 0, screenSize["height"], screenSize["gridWidth"] do
    for x = 0, screenSize["width"], screenSize["gridWidth"] do
      images["ground"]:draw( x, y )
    end
  end
end

function GetRandomX()
  return math.random( 0, screenSize["width"] - screenSize["gridWidth"] )
end

function GetRandomY()
  return math.random( screenSize["hudHeight"], screenSize["height"] - screenSize["gridWidth"] )
end

function KeepInBounds( obj )
  if ( obj["x"] < 0 ) then obj["x"] = 0 end
  
  if ( obj["x"] > screenSize["width"] - screenSize["gridWidth"] ) then
    obj["x"] = screenSize["width"] - screenSize["gridWidth"]
  end
  
  if ( obj["y"] < screenSize["hudHeight"] ) then obj["y"] = screenSize["hudHeight"] end
  
  if ( obj["y"] > screenSize["height"] - screenSize["gridWidth"] ) then
    obj["y"] = screenSize["height"] - screenSize["gridWidth"]
  end
end

function UpdatePlayer()
  playerObject["frame"] = playerObject["frame"] + playerObject["frameSpeed"]
  if ( playerObject["frame"] >= playerObject["frameMax"] ) then
    playerObject["frame"] = 0
  end
  
  playerObject["x"] = playerObject["x"] + (playerObject["velocityX"]*playerObject["walkSpeed"])
  playerObject["y"] = playerObject["y"] + (playerObject["velocityY"]*playerObject["walkSpeed"])
end

function DrawPlayer()
  playerObject["image"]:draw( playerObject["x"], playerObject["y"], gfx.kImageUnflipped, math.floor( playerObject["frame"] ) * playerObject["width"], playerObject["direction"] * playerObject["height"], playerObject["width"], playerObject["height"] )
end

function PlayerAccelerate( direction )
  playerObject["direction"] = sheetDirection[ direction ]
  if ( direction == "up" ) then
    playerObject["velocityY"] = -1
  elseif ( direction == "down" ) then
    playerObject["velocityY"] = 1
  elseif ( direction == "left" ) then
    playerObject["velocityX"] = -1
  elseif ( direction == "right" ) then
    playerObject["velocityX"] = 1
  end
end

function PlayerDeaccelerate( direction )
  if ( direction == "up" or direction == "down" ) then
    playerObject["velocityY"] = 0
  elseif ( direction == "left" or direction == "right" ) then
    playerObject["velocityX"] = 0
  end
end

function IsCollision( objA, objB )
  return (  objA["x"]                   <= objB["x"] + objB["width"] and
            objA["x"] + objA["width"]   >= objB["x"] + objB["width"] and
            objA["y"]                   <= objB["y"] + objB["height"] and
            objA["y"] + objA["height"]  >= objB["y"] + objB["height"] )
end

stickObject = {}
stickObject["x"] = GetRandomX()
stickObject["y"] = GetRandomY()
stickObject["width"] = 20
stickObject["height"] = 20
stickObject["image"] = gfx.image.new( "images/stick.png" )

playerObject = {}
playerObject["x"] = GetRandomX()
playerObject["y"] = GetRandomY()
playerObject["width"] = 20
playerObject["height"] = 20
playerObject["image"] = gfx.image.new( "images/girl.png" )
playerObject["score"] = 0
playerObject["walkSpeed"] = 2
playerObject["direction"] = 0
playerObject["frame"] = 0
playerObject["frameMax"] = 2
playerObject["frameSpeed"] = 0.1
playerObject["velocityX"] = 0
playerObject["velocityY"] = 0

function playdate.update()
  -- Updates
  UpdatePlayer()
  KeepInBounds( playerObject )
  KeepInBounds( stickObject )
    
  if ( IsCollision( playerObject, stickObject ) ) then
    stickObject["x"] = GetRandomX()
    stickObject["y"] = GetRandomY()
    playerObject["score"] = playerObject["score"] + 1
    soundCollect:play()
  end

  
  -- Drawing
  gfx.clear( gfx.kColorWhite )
  gfx.setColor( gfx.kColorBlack )
  DrawGround()
  stickObject["image"]:draw( stickObject["x"], stickObject["y"] )
  DrawPlayer()
  
  gfx.setColor( gfx.kColorWhite )
  gfx.fillRect( 0, 0, screenSize["width"], screenSize["hudHeight"] )
  gfx.setColor( gfx.kColorBlack )
  gfx.drawLine( 0, screenSize["hudHeight"], screenSize["width"], screenSize["hudHeight"] )
  gfx.setColor( gfx.kColorBlack )
  gfx.drawText( "Score: " .. playerObject["score"], 2, 2 )
  gfx.drawText( "Pickin' Sticks", 300, 2 )
end

function playdate.leftButtonDown()    PlayerAccelerate( "left" )  end
function playdate.leftButtonUp()      PlayerDeaccelerate( "left" )  end
                 
function playdate.rightButtonDown()   PlayerAccelerate( "right" )  end
function playdate.rightButtonUp()     PlayerDeaccelerate( "right" )  end
                      
function playdate.upButtonDown()      PlayerAccelerate( "up" )  end
function playdate.upButtonUp()        PlayerDeaccelerate( "up" )  end
                            
function playdate.downButtonDown()    PlayerAccelerate( "down" )  end
function playdate.downButtonUp()      PlayerDeaccelerate( "down" )  end
