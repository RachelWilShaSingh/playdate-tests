-- https://gitlab.com/RachelWilShaSingh/playdate-tests
-- Rachel Singh
-- This is just messing around with trigonometry, not really any point lol

import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'

print( "Example ~ Blop? ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )
screenSize = { width = 400, height = 240 }

speed = 1

x = 0
y = 240/2

lastX = x
lastY = y

pointList = {}

function playdate.update()
  gfx.clear( gfx.kColorWhite )
  
  -- Colors:
  -- playdate.graphics.kColorBlack
  -- playdate.graphics.kColorWhite
  -- playdate.graphics.kColorClear
  -- playdate.graphics.kColorXOR
  
  crankAngleDegrees = playdate.getCrankPosition() - 90
  crankAngleRadians = math.rad( crankAngleDegrees )
  crankChangeDegrees = playdate.getCrankChange()
  
  cosine = math.cos( crankAngleRadians )
  sine = math.sin( crankAngleRadians )
  
  radius = screenSize["height"] / 2
  
  x = x + speed
  y = 240/2 + (sine * radius)
  if ( x > 400 ) then
    x = -5
    -- Clear point list
    for key in pairs( pointList ) do
      pointList[key] = nil
    end
  end
  
  table.insert( pointList, { px = x, py = y } )

  gfx.setColor( gfx.kColorBlack ) 
  
  gfx.drawLine( lastX, lastY, x, y )
    
  for key, point in pairs( pointList ) do
    --gfx.drawPixel( point["px"], point["py"] )
    gfx.fillCircleAtPoint( point["px"], point["py"], 5 )
  end
   
  gfx.drawCircleAtPoint( x, y, 5 )

  lastX = x
  lastY = y  
end
