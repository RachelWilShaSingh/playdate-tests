import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'
print( "Example ~ MoosieTron ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

------------------------------------------------------------------------ Functions

function GetRandomX() return math.random( 0, screenSize["width"] - screenSize["gridWidth"] ) end
function GetRandomY() return math.random( screenSize["hudHeight"], screenSize["height"] - screenSize["gridWidth"] ) end

function DrawCharacter( char )
  char["image"]:draw( char["x"], char["y"] )
end

function UpdateBullet( char )
  char["y"] = char["y"] + char["speed"]
  if ( char["y"] < 0 ) then
    return "DESTROY"
  else
    return ""
  end
end

function ShootBullet( shootSource, bulletObjects )
  if ( shootSource["bulletCooldown"] > 0 ) then return end
  bullet = BulletBuilder( shootSource )
  table.insert( bulletObjects, bullet )
  shootSource["bulletCooldown"] = shootSource["bulletCooldownMax"]
  --sounds["shoot"]:play()
end

function BulletBuilder( shootSource )
  bullet = {}
  bullet["width"] = 5
  bullet["height"] = 10
  bullet["image"] = images["bullet"]
  bullet["x"] = shootSource["x"] + shootSource["width"] / 2
  bullet["y"] = shootSource["y"]
  bullet["speed"] = -2
  bullet["angle"] = shootSource["crankAngleRadians"]
  return bullet
end

function DrawBullets( bulletObjects )
  for key, bullet in pairs( bulletObjects ) do
    DrawCharacter( bullet )
  end
end

function UpdateBullets( bulletObjects )
  deleteMeKeys = {}
  
  for key, bullet in pairs( bulletObjects ) do
    oldX = bullet["x"]
    oldY = bullet["y"]
    
    cosine = math.cos( bullet["angle"] )
    sine = math.sin( bullet["angle"] )
    
    newX = oldX - (cosine * bullet["speed"])
    newY = oldY - (sine * bullet["speed"])
    
    bullet["x"] = newX
    bullet["y"] = newY
    
    if ( CheckIfOffScreen( bullet ) == true ) then
      table.insert( deleteMeKeys, key )
    end
  end
  
  -- Delete any bullets off screen
  for k, bulletKey in pairs( deleteMeKeys ) do
    DestroyElement( bulletObjects, bulletKey )
  end
end

function UpdatePlayer( playerObject, buttonPressing )
  if ( playerObject["bulletCooldown"] >= 0 ) then
    playerObject["bulletCooldown"] = playerObject["bulletCooldown"] - 1
  else
    playerObject["bulletCooldown"] = 0
  end
  
  if ( buttonPressing["up"] ) then
    playerObject["y"] = playerObject["y"] - playerObject["speed"]
  elseif ( buttonPressing["down"] ) then
    playerObject["y"] = playerObject["y"] + playerObject["speed"]
  elseif ( buttonPressing["left"] ) then
    playerObject["x"] = playerObject["x"] - playerObject["speed"]
  elseif ( buttonPressing["right"] ) then
    playerObject["x"] = playerObject["x"] + playerObject["speed"]
  end
  
  print( playerObject["x"] .. ", " .. playerObject["y"] )
end

function CrankUpdate( playerObject )
  playerObject["crankAngleDegrees"] = playdate.getCrankPosition() - 90
  playerObject["crankAngleRadians"] = math.rad( playerObject["crankAngleDegrees"] )
  playerObject["crankChangeDegrees"] = playdate.getCrankChange()
  
  cosine = math.cos( playerObject["crankAngleRadians"] )
  sine = math.sin( playerObject["crankAngleRadians"] )
  
  radius = 50
  
  x1 = playerObject["x"] + playerObject["width"] / 2
  y1 = playerObject["y"] + playerObject["height"] / 2
  x2 = x1 + (cosine * radius)
  y2 = y1 + (sine * radius)
  
  gfx.setColor( gfx.kColorBlack )
  gfx.drawLine( x1, y1, x2, y2 )
end

function CheckIfOffScreen( object )
  return ( object["x"] < 0 - object["width"] or object["x"] > screenSize["width"] or object["y"] < 0 - object["height"] or object["y"] > screenSize["height"] )
end

function DestroyElement( objects, key )
  objects[key] = nil
end

------------------------------------------------------------------------ Game object initialization

screenSize = { width = 400, height = 240, gridWidth = 20, hudHeight = 20 }
gfx.setBackgroundColor( gfx.kColorBlack )

images = {
  player = gfx.image.new( "images/robot.png" ),
  bullet = gfx.image.new( "images/bullet.png" )
}

bulletObjects = {}

playerObject = {}
playerObject["width"] = 36
playerObject["height"] = 41
playerObject["x"] = screenSize["width"] / 2 - playerObject["width"] / 2
playerObject["y"] = screenSize["height"] - playerObject["height"]
playerObject["image"] = images["player"]
playerObject["score"] = 0
playerObject["speed"] = 2
playerObject["bulletCooldown"] = 0
playerObject["bulletCooldownMax"] = 25
playerObject["aimAngle"] = 0
playerObject["aimLength"] = 25


buttonPressing = {
  up    = false,
  down  = false,
  left  = false,
  right = false,
  a     = false,
  b     = false
}

------------------------------------------------------------------------ Update function

function playdate.update()    
  gfx.clear( gfx.kColorWhite )
  gfx.setColor( gfx.kColorWhite )
  
  UpdateBullets( bulletObjects )
  UpdatePlayer( playerObject, buttonPressing )
    
  DrawCharacter( playerObject )
  DrawBullets( bulletObjects )
  
  CrankUpdate( playerObject )

  gfx.setColor( gfx.kColorWhite )
  gfx.fillRect( 0, 0, screenSize["width"], screenSize["hudHeight"] )
  gfx.drawText( "MoosieTron", 270, 0 )  
end

------------------------------------------------------------------------ Callback functions

function playdate.upButtonDown()    buttonPressing["up"] = true       end
function playdate.upButtonUp()      buttonPressing["up"] = false      end
function playdate.downButtonDown()  buttonPressing["down"] = true     end
function playdate.downButtonUp()    buttonPressing["down"] = false    end                                    
function playdate.leftButtonDown()  buttonPressing["left"] = true     end
function playdate.leftButtonUp()    buttonPressing["left"] = false    end                                    
function playdate.rightButtonDown() buttonPressing["right"] = true    end
function playdate.rightButtonUp()   buttonPressing["right"] = false   end

function playdate.BButtonDown()     ShootBullet( playerObject, bulletObjects )  end
