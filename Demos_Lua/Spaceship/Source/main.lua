import 'CoreLibs/graphics.lua'
import 'CoreLibs/sprites.lua'
import 'CoreLibs/crank.lua'

print( "Example ~ Spaceship ~ RWSS" )

local gfx = playdate.graphics
local snd = playdate.sound

playdate.display.setRefreshRate( 50 )

screenSize = { width = 400, height = 240, gridWidth = 20, hudHeight = 20 }

images = {}
images["ship"] = gfx.image.new( "images/ship" )

entities = {}
entities["ship"] = {} 
entities["ship"]["vel"] = 0
entities["ship"]["maxVel"] = 5
entities["ship"]["acc"] = 1
entities["ship"]["sprite"] = gfx.sprite.new( images["ship"] )
entities["ship"]["sprite"]:moveTo( screenSize["width"]/2, screenSize["height"]/2 )
entities["ship"]["sprite"]:setVisible( true )
entities["ship"]["sprite"]:setCenter( 0.5, 0.5 )
entities["ship"]["sprite"]:add()

gfx.setBackgroundColor( gfx.kColorBlack )

function Accelerate( direction )
  entities["ship"]["vel"] += direction * entities["ship"]["acc"]
  if ( entities["ship"]["vel"] > entities["ship"]["maxVel"] ) then
    entities["ship"]["vel"] = entities["ship"]["maxVel"]
  end
end

function GetRandomX()
  return math.random( 0, screenSize["width"] - screenSize["gridWidth"] )
end

function GetRandomY()
  return math.random( screenSize["hudHeight"], screenSize["height"] - screenSize["gridWidth"] )
end

function KeepSpriteInBounds( obj )
  if ( obj.x < 0 ) then 
    obj.x = screenSize["width"]
  elseif ( obj.x > screenSize["width"] ) then
    obj.x = 0
  end
  
  if ( obj.y < 0 ) then
    obj.y = screenSize["height"]
  elseif ( obj.y > screenSize["height"] ) then
    obj.y = 0
  end
  
  obj:moveTo( obj.x, obj.y )
end

function PlayerMovement( degrees )
  -- Get angle in radians, from degrees:
  local rads = math.rad( degrees - 90 ) -- PlayDate angle 0 is "up"
  x = -math.cos( rads )
  y = -math.sin( rads )
  velocity = entities["ship"]["vel"]
  entities["ship"]["sprite"]:moveBy( x * velocity, y * velocity )
  KeepSpriteInBounds( entities["ship"]["sprite"] )
  --print( "Degrees:", degrees, "Radians:", rads, "x:", x, "y:", y, "pos:", entities["ship"]["sprite"].x, entities["ship"]["sprite"].y )
end

function playdate.update()
  -- GAME UPDATE
  -- Use crank to rotate ship
  crankDegrees = playdate.getCrankPosition()
  entities["ship"]["sprite"]:setRotation( crankDegrees )
  
  -- Use UP/DOWN to accelerate/deaccelerate
  if ( playdate.buttonIsPressed( playdate.kButtonUp ) ) then
    Accelerate( 1 )
  elseif ( playdate.buttonIsPressed( playdate.kButtonDown ) ) then
    Accelerate( -1 )
  end
  
  -- Use any other key to shoot
  if ( playdate.buttonIsPressed( playdate.kButtonA ) or
       playdate.buttonIsPressed( playdate.kButtonB ) or
       playdate.buttonIsPressed( playdate.kButtonLeft ) or
       playdate.buttonIsPressed( playdate.kButtonRight ) ) then
       
  end
  
  -- Update player position
  PlayerMovement( crankDegrees )
  
  -- DRAWING
  gfx.clear( gfx.kColorBlack )
  gfx.sprite.update() -- Update ALL sprite objects

  --gfx.drawText( "Score: " .. playerObject["score"], 2, 2 )
  --gfx.drawText( "Pickin' Sticks", 300, 2 )
end


