import 'CoreLibs/graphics.lua'
print( "Example ~ Random Dungeon ~ RWSS" )

local gfx = playdate.graphics
local snd = playdate.sound

------------------------------------------------------------------------ Functions
function IsCollision( objA, objB )
  return (  objA["x"]                   <= objB["x"] + objB["width"] and
            objA["x"] + objA["width"]   >= objB["x"] + objB["width"] and
            objA["y"]                   <= objB["y"] + objB["height"] and
            objA["y"] + objA["height"]  >= objB["y"] + objB["height"] )
end

function GetRandom( minval, maxval )
  return math.random( minval, maxval )
end

function GenerateMap()
  tilesWide = screenSize["width"] / screenSize["gridWidth"]
  tilesHigh = screenSize["height"] / screenSize["gridWidth"]
  
  ceilingTile = 0
  wallTile = 1
  floorTile = 2
  
  for y = 0, tilesHigh, 1 do
    for x = 0, tilesWide, 1 do
      key = tostring( x ) .. "-" .. tostring( y )
      
      posX = x * screenSize["gridWidth"]
      posY = y * screenSize["gridWidth"]
          
      mapTiles[ key ] = {}
      mapTiles[ key ]["x"] = posX
      mapTiles[ key ]["y"] = posY
      mapTiles[ key ]["width"] = 20
      mapTiles[ key ]["height"] = 20
      mapTiles[ key ]["image"] = images["tileset"]
      -- Begin every tile as the ceiling
      mapTiles[ key ]["framex"] = ceilingTile
      mapTiles[ key ]["framey"] = 0
      mapTiles[ key ]["walkable"] = false
    end
  end
  
  -- Create rooms
  rooms = {}
  for r = 1, 5 do
    x = GetRandom( 0, tilesWide-1 )
    y = GetRandom( 0, tilesHigh-1 )
    key = tostring( x ) .. "-" .. tostring( y )
    mapTiles[ key ]["framex"] = wallTile
    
    room = {}
    room["x"] = x
    room["y"] = y
    
    table.insert( rooms, room )
  end
  
  print( "Room size", #rooms )
  
  -- Link rooms
  for idx1 = 1, #rooms - 1 do
    room1 = rooms[idx1]
    room2 = rooms[idx1+1]
        
    --y = room1["y"]
    --for x = room1["x"], room2["x"] do
      --key = tostring( x ) .. "-" .. tostring( y )
      --mapTiles[ key ]["framex"] = floorTile
      --print( key )
    --end
    --for y = room1["y"], room2["y"] do
      --key = tostring( x ) .. "-" .. tostring( y )
      --mapTiles[ key ]["framex"] = floorTile
      --print( key )
    --end
  end
end

function DrawMap()
  for key, tile in pairs( mapTiles ) do
    tile["image"]:draw( tile["x"], tile["y"], gfx.kImageUnflipped, math.floor( tile["framex"] ) * tile["width"], tile["framey"] * tile["height"], tile["width"], tile["height"] )
  end
end

------------------------------------------------------------------------ Initialize data
playdate.display.setRefreshRate( 50 )

screenSize = { width = 400, height = 240, gridWidth = 20, hudHeight = 20 }

images = {
  tileset = gfx.image.new( "images/tileset.png" ),
  girl    = gfx.image.new( "images/girl.png" ),
  candy   = gfx.image.new( "images/candy.png" )
}

sounds = {
}

mapTiles = {}
GenerateMap()

sheetDirection = {}
sheetDirection["left"] = 0
sheetDirection["up"] = 1
sheetDirection["right"] = 2
sheetDirection["down"] = 3

playerObject = {}
playerObject["x"] = 0--GetRandomX()
playerObject["y"] = 0--GetRandomY()
playerObject["width"] = 20
playerObject["height"] = 20
playerObject["image"] = images["girl"]
playerObject["score"] = 0
playerObject["walkSpeed"] = 2
playerObject["direction"] = 0
playerObject["frame"] = 0
playerObject["frameMax"] = 2
playerObject["frameSpeed"] = 0.1
playerObject["velocityX"] = 0
playerObject["velocityY"] = 0

gfx.setBackgroundColor( gfx.kColorWhite )

------------------------------------------------------------------------ Game loop

function playdate.update()
  -- Updates
  --UpdatePlayer()
  --KeepInBounds( playerObject )
  --KeepInBounds( stickObject )
    
  --if ( IsCollision( playerObject, stickObject ) ) then
    --stickObject["x"] = GetRandomX()
    --stickObject["y"] = GetRandomY()
    --playerObject["score"] = playerObject["score"] + 1
    --soundCollect:play()
  --end

  
  -- Drawing
  gfx.clear( gfx.kColorWhite )
  gfx.setColor( gfx.kColorBlack )
  
  DrawMap()
  
  --stickObject["image"]:draw( stickObject["x"], stickObject["y"] )
  --DrawPlayer()
  
  gfx.setColor( gfx.kColorWhite )
  gfx.fillRect( 0, 0, screenSize["width"], screenSize["hudHeight"] )
  gfx.drawText( "Random Dungeon", 270, 2 )
  --gfx.setColor( gfx.kColorBlack )
  --gfx.drawLine( 0, screenSize["hudHeight"], screenSize["width"], screenSize["hudHeight"] )
  --gfx.setColor( gfx.kColorBlack )
  --gfx.drawText( "Score: " .. playerObject["score"], 2, 2 )
end

------------------------------------------------------------------------ Event callbacks

function playdate.leftButtonDown()    PlayerAccelerate( "left" )  end
function playdate.leftButtonUp()      PlayerDeaccelerate( "left" )  end
                 
function playdate.rightButtonDown()   PlayerAccelerate( "right" )  end
function playdate.rightButtonUp()     PlayerDeaccelerate( "right" )  end
                      
function playdate.upButtonDown()      PlayerAccelerate( "up" )  end
function playdate.upButtonUp()        PlayerDeaccelerate( "up" )  end
                            
function playdate.downButtonDown()    PlayerAccelerate( "down" )  end
function playdate.downButtonUp()      PlayerDeaccelerate( "down" )  end
