import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'
print( "Example ~ Gorillas dot pd ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

------------------------------------------------------------------------ Functions

function GetRandomX() return math.random( 0, screenSize["width"] - screenSize["gridWidth"] ) end
function GetRandomY() return math.random( screenSize["hudHeight"], screenSize["height"] - screenSize["gridWidth"] ) end

function DrawCharacter( char )
  char["image"]:draw( char["x"], char["y"] )
end

function ShootBullet( shootSource, bulletObjects )
  if ( shootSource["bulletCooldown"] > 0 ) then return end
  bullet = BulletBuilder( shootSource )
  table.insert( bulletObjects, bullet )
  shootSource["bulletCooldown"] = shootSource["bulletCooldownMax"]
  --sounds["shoot"]:play()
end

function BulletBuilder( shootSource )
  bullet = {}
  bullet["width"] = 5
  bullet["height"] = 10
  bullet["image"] = images["bullet"]
  bullet["x"] = shootSource["x"] + shootSource["width"] / 2
  bullet["y"] = shootSource["y"]
  bullet["initialVelocity"] = -7
  bullet["angle"] = shootSource["crankAngleRadians"]
  bullet["time"] = 0
  bullet["timeInc"] = 0.1
  bullet["tracker"] = {}
  return bullet
end

function DrawBullets( bulletObjects )
  for key, bullet in pairs( bulletObjects ) do
    DrawCharacter( bullet )
    
    for pk, pixel in pairs( bullet["tracker"] ) do
      gfx.setColor( gfx.kColorBlack )
      gfx.drawPixel( pixel["x"], pixel["y"] )
    end
  end
end

function UpdateBullets( bulletObjects )
  deleteMeKeys = {}
  
  for key, bullet in pairs( bulletObjects ) do
    bullet["time"] = bullet["time"] + bullet["timeInc"]
  
    oldX = bullet["x"]
    oldY = bullet["y"]
    
    cosine = math.cos( bullet["angle"] )
    sine = math.sin( bullet["angle"] )
    
    newX = oldX - (cosine * bullet["initialVelocity"])
    newY = oldY - (sine * bullet["initialVelocity"])
    
    gravityY = gravity * bullet["time"]
    
    bullet["x"] = newX
    bullet["y"] = newY - gravityY
    
    table.insert( bullet["tracker"], { x = bullet["x"], y = bullet["y"] } )
    
    if ( CheckIfOffScreen( bullet ) == true ) then
      table.insert( deleteMeKeys, key )
    end
  end
  
  -- Delete any bullets off screen
  for k, bulletKey in pairs( deleteMeKeys ) do
    DestroyElement( bulletObjects, bulletKey )
  end
end

function UpdatePlayer( playerObject )
  if ( playerObject["bulletCooldown"] >= 0 ) then
    playerObject["bulletCooldown"] = playerObject["bulletCooldown"] - 1
  else
    playerObject["bulletCooldown"] = 0
  end
end

function CrankUpdate( playerObject )
  playerObject["crankAngleDegrees"] = playdate.getCrankPosition() - 90
  playerObject["crankAngleRadians"] = math.rad( playerObject["crankAngleDegrees"] )
  playerObject["crankChangeDegrees"] = playdate.getCrankChange()
  
  cosine = math.cos( playerObject["crankAngleRadians"] )
  sine = math.sin( playerObject["crankAngleRadians"] )
  
  radius = 50
  
  x1 = playerObject["x"] + playerObject["width"] / 2
  y1 = playerObject["y"]
  x2 = x1 + (cosine * radius)
  y2 = y1 + (sine * radius)
  
  gfx.setColor( gfx.kColorBlack )
  gfx.drawLine( x1, y1, x2, y2 )
end

function CheckIfOffScreen( object )
  return ( object["x"] < 0 - object["width"] or object["x"] > screenSize["width"] or object["y"] < 0 - object["height"] or object["y"] > screenSize["height"] )
end

function DestroyElement( objects, key )
  objects[key] = nil
end

------------------------------------------------------------------------ Game object initialization

screenSize = { width = 400, height = 240, gridWidth = 20, hudHeight = 20 }
gfx.setBackgroundColor( gfx.kColorBlack )

images = {
  gorilla = gfx.image.new( "images/gorilla.png" ),
  bullet  = gfx.image.new( "images/banana.png" )
}

gravity = -2

bulletObjects = {}

playerObject = {}
playerObject["width"] = 36
playerObject["height"] = 41
playerObject["x"] = screenSize["width"] / 2 - playerObject["width"] / 2
playerObject["y"] = screenSize["height"] - playerObject["height"]
playerObject["image"] = images["gorilla"]
playerObject["score"] = 0
playerObject["bulletCooldown"] = 0
playerObject["bulletCooldownMax"] = 10
playerObject["aimAngle"] = 0
playerObject["aimLength"] = 25

------------------------------------------------------------------------ Update function

function playdate.update()    
  gfx.clear( gfx.kColorWhite )
  gfx.setColor( gfx.kColorWhite )
  
  -- Update any existing bullets
  UpdateBullets( bulletObjects )
  
  -- Update the Gorilla
  UpdatePlayer( playerObject )
    
  -- Draw the Gorilla
  DrawCharacter( playerObject )
  
  -- Draw the bullets
  DrawBullets( bulletObjects )
  
  -- Update and draw an aiming line
  CrankUpdate( playerObject )

  gfx.setColor( gfx.kColorWhite )
  gfx.fillRect( 0, 0, screenSize["width"], screenSize["hudHeight"] )
  gfx.drawText( "Gorillas dot PD", 270, 0 )  
end

------------------------------------------------------------------------ Callback functions

--function playdate.leftButtonDown()    HandleMovement( playerObject, "LEFT" )              end
--function playdate.leftButtonUp()      HandleMovement( playerObject, "" )                  end
--function playdate.rightButtonDown()   HandleMovement( playerObject, "RIGHT" )             end
--function playdate.rightButtonUp()     HandleMovement( playerObject, "" )                  end
function playdate.BButtonDown()       ShootBullet( playerObject, bulletObjects )                         end
