import 'utilities.lua'
import 'screen_mainmenu.lua'

import 'packages/test1.lua' 
import 'packages/test2.lua' 
import 'packages/test3.lua' 
import 'packages/test4.lua' 
import 'packages/test5.lua' 

currentState = nil

stateMap = {
  -- This is kind of weird... the key is technically a string,
  -- and the value is an actual reference to another table (a game screen).
  mainMenuScreen          = mainMenuScreen,
                          
  state_test1       = state_test1,
  state_test2       = state_test2,
  state_test3       = state_test3,
  state_test4       = state_test4,
  state_test5       = state_test5,
}

function ChangeState( nextState )  
  if ( currentState ~= nil ) then
    currentState.Cleanup()    
  end
  
  if ( DoesTableContainKey( stateMap, nextState ) ) then
    currentState = stateMap[nextState]
  end
  
  InitState()
end

function InitState()
  if ( currentState ~= nil ) then
    currentState.Init()
  end
end

function UpdateState()
  if ( currentState ~= nil ) then
    currentState.Update()
    
    if ( currentState.nextState ~= "" ) then
      ChangeState( currentState.nextState )
    end
  end
end

function DrawState()
  if ( currentState ~= nil ) then
    currentState.Draw()
  end
end
