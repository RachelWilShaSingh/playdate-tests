import 'CoreLibs/graphics.lua'
local gfx = playdate.graphics

mainMenuScreen = {
  key = "title",    -- A string identifier for this state
  nextState = "",   -- Set this when you're ready to go to another state
  version = "1.1",
  
  imgBackground = gfx.image.new( "images/menubgmain.png" ),
  imgCursor     = gfx.image.new( "images/cursor.png" ),
  imgButton     = gfx.image.new( "images/buttonbgbig.png" ),
  
  buttons = {
    {
      text = "Image draw stress",
      x = 25, 
      y = 50,
      width = 180,
      height = 30,
      textOffsetX = 0,
      textOffsetY = 0,
      cursorOffsetX = -20,
      gotoState = "state_test1"
    },
    {
      text = "Large image test",
      x = 25,
      y = 75,
      width = 180,
      height = 30,
      textOffsetX = 0,
      textOffsetY = 0,
      cursorOffsetX = -20,
      gotoState = "state_test2"
    },
    {
      text = "Image + Image Table test",
      x = 25,
      y = 100,
      width = 180,
      height = 30,
      textOffsetX = 0,
      textOffsetY = 0,
      cursorOffsetX = -20,
      gotoState = "state_test3"
    },
    {
      text = "Game slowdown debug",
      x = 25,
      y = 125,
      width = 180,
      height = 30,
      textOffsetX = 0,
      textOffsetY = 0,
      cursorOffsetX = -20,
      gotoState = "state_test4"
    },
    {
      text = "Game slowdown debug B",
      x = 25,
      y = 150,
      width = 180,
      height = 30,
      textOffsetX = 0,
      textOffsetY = 0,
      cursorOffsetX = -20,
      gotoState = "state_test5"
    },
  },
  
  cursor = {
    width = 25,
    height = 25,
    positions = { 
    },
    currentPosition = 1
  },
  
  Init = function( self )
    mainMenuScreen.nextState = ""
  end,
  
  Update = function( self )
  end,
  
  Draw = function( self )
    mainMenuScreen.imgBackground:draw( 0, 0 )
    
    --gfx.setColor( gfx.kColorWhite )
    --gfx.fillRect( 400*1/4, 0, 400*2/4, 20 )
    gfx.drawTextAligned( "TESTER v" .. mainMenuScreen.version, 400/2-23, 8, kTextAlignment.left )
    
    -- Draw buttons
    for key, button in pairs( mainMenuScreen.buttons ) do
      gfx.drawTextAligned( button["text"], button["x"] + button["textOffsetX"], button["y"] + button["textOffsetY"], kTextAlignment.left )
    end
    
    -- Draw cursor
    local selection = mainMenuScreen.cursor.currentPosition
    local button = mainMenuScreen.buttons[ selection ]
    mainMenuScreen.imgCursor:draw( button["x"] + button["cursorOffsetX"], button["y"] )
  end,
  
  Cleanup = function( self )
    -- Free space here as needed
  end,
  
  Handle_upButtonDown = function( self )
    mainMenuScreen.cursor["currentPosition"] = mainMenuScreen.cursor["currentPosition"] - 1
    if ( mainMenuScreen.cursor["currentPosition"] == 0 ) then
      mainMenuScreen.cursor["currentPosition"] = #mainMenuScreen.buttons
    end
  end,
  
  -- Button handlers
  Handle_downButtonDown = function( self )
    mainMenuScreen.cursor["currentPosition"] = mainMenuScreen.cursor["currentPosition"] + 1
    if ( mainMenuScreen.cursor["currentPosition"] > #mainMenuScreen.buttons ) then
      mainMenuScreen.cursor["currentPosition"] = 1
    end
  end,
  
  Handle_AButtonDown = function( self )
    local cursorPosition = mainMenuScreen.cursor["currentPosition"]
    local selection = mainMenuScreen.cursor.currentPosition
    local button = mainMenuScreen.buttons[ selection ]
    mainMenuScreen.nextState = button["gotoState"]
  end,
  
  Handle_leftButtonDown   = function( self ) 
    print( "A currentPosition:", mainMenuScreen.cursor["currentPosition"] )
    mainMenuScreen.cursor["currentPosition"] += #mainMenuScreen.buttons / 2
    
    if ( mainMenuScreen.cursor["currentPosition"] > #mainMenuScreen.buttons ) then
      mainMenuScreen.cursor["currentPosition"] -= #mainMenuScreen.buttons 
    end
    --mainMenuScreen.cursor["currentPosition"] = (mainMenuScreen.cursor["currentPosition"] % #mainMenuScreen.buttons)
     
    print( "B currentPosition:", mainMenuScreen.cursor["currentPosition"] )
  end,
  
  Handle_rightButtonDown  = function( self )
    print( "A currentPosition:", mainMenuScreen.cursor["currentPosition"] )
    mainMenuScreen.cursor["currentPosition"] += #mainMenuScreen.buttons / 2
    
    if ( mainMenuScreen.cursor["currentPosition"] > #mainMenuScreen.buttons ) then
      mainMenuScreen.cursor["currentPosition"] -= #mainMenuScreen.buttons 
    end
    --mainMenuScreen.cursor["currentPosition"] = (mainMenuScreen.cursor["currentPosition"] % #mainMenuScreen.buttons)
    print( "B currentPosition:", mainMenuScreen.cursor["currentPosition"] )
  end,
  
  
  -- Unused
  Handle_upButtonUp       = function( self ) 
  end,
  
  Handle_downButtonUp     = function( self ) 
  end,
  
  Handle_leftButtonUp     = function( self ) 
  end,
  
  Handle_rightButtonUp    = function( self ) 
  end,
  
  Handle_BButtonDown      = function( self ) 
  end,
  
  Handle_AButtonUp        = function( self ) 
  end,
  
  Handle_BButtonUp        = function( self ) 
  end,
}
