import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'
local gfx = playdate.graphics

state_test1 = {
  nextState = "",   -- Set this when you're ready to go to another state
  
  keypresses = { up = false, down = false, left = false, right = false, a = false, b = false },
    
  image = gfx.image.new( "images/test1/face.png" ),
  
  objects = {},
  
  Init = function( self )
    state_test1.nextState = ""
    table.insert( state_test1["objects"], { x = math.random( 0, 400 ), y = math.random( 0, 240 ) } )
  end,
  
  Update = function( self )
    for key, obj in pairs( state_test1["objects"] ) do
      obj["x"] += 5
      obj["y"] += 5
      
      if ( obj["x"] > 400 ) then obj["x"] = -24 end
      if ( obj["y"] > 240 ) then obj["y"] = -24 end
    end
    
    crankChangeDegrees = playdate.getCrankChange()
    if ( crankChangeDegrees > 0 ) then
      table.insert( state_test1["objects"], { x = math.random( 0, 400 ), y = math.random( 0, 240 ) } )
    elseif ( crankChangeDegrees < 0 ) then
      local objectCount = #state_test1["objects"]
      state_test1["objects"][objectCount] = nil
    end
  end,
  
  Draw = function( self )
    gfx.clear( gfx.kColorWhite )
    gfx.setColor( gfx.kColorWhite )
    gfx.drawRect( 0, 0, 400, 240 )
    
    for key, obj in pairs( state_test1["objects"] ) do
      state_test1["image"]:draw( obj["x"], obj["y"] )
    end
    
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 20 )
    gfx.drawText( "Use crank to add/remove", 5, 5 )
    gfx.drawText( "Object count: " .. tostring( #state_test1.objects ), 220, 5 )
    playdate.drawFPS( 380, 5 )
  end,
  
  Cleanup = function( self )
  end,
  
  
  -- Button handlers
  Handle_leftButtonDown = function( self )
  end,
  
  Handle_rightButtonDown = function( self )
  end,
  
  Handle_AButtonDown = function( self )
  end,
  
  Handle_BButtonDown      = function( self ) 
  end,
  
  
  -- Unused
  Handle_upButtonUp       = function( self ) 
  end,
  
  Handle_downButtonUp     = function( self ) 
  end,
  
  Handle_downButtonDown   = function( self ) 
  end,
  
  Handle_leftButtonUp     = function( self ) 
  end,
  
  Handle_upButtonDown  = function( self ) 
  end,
  
  Handle_rightButtonUp    = function( self ) 
  end,
  
  Handle_AButtonUp        = function( self ) 
  end,
  
  Handle_BButtonUp        = function( self ) 
  end,
}
