import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'
local gfx = playdate.graphics

state_test2 = {
  nextState = "",   -- Set this when you're ready to go to another state
  
  keypresses = { up = false, down = false, left = false, right = false, a = false, b = false },
    
  images = {
    background = gfx.image.new( "images/test2/background.png" ),
    face = gfx.image.new( "images/test1/face.png" ),
  },
  
  tables = {
    fist = nil
  },
  
  backgroundWidth = 1600,
  
  view = {
    turnSpeed = 1,
    xOffset = 0,
    viewXMin = 0,
    viewXMax = 0,
    spawnMinX = 400,
    spawnMaxX = 1600-400
  },
  
  Init = function( self )
    state_test2.nextState = ""
    
    state_test2.tables["fist"], result = gfx.imagetable.new( "images/test2/punch" )
    if ( result ~= nil ) then print( "ERROR:", result ) end
  end,
  
  Update = function( self )
    state_test2.Handle_Crank()
  end,
  
  Draw = function( self )
    local offset = state_test2["view"]["xOffset"]
    local bgWidth = state_test2["backgroundWidth"]
    
    gfx.clear( gfx.kColorWhite )
    gfx.setColor( gfx.kColorWhite )

    state_test2["images"]["background"]:draw( offset, 0 )
    state_test2["images"]["background"]:draw( offset + bgWidth, 0 )
    state_test2["images"]["face"]:draw( 100, 100 )
    
    state_test2.tables["fist"]:getImage( 1 ):draw( 278, 75 )
    
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 20 )
    gfx.drawText( "Use crank to move", 5, 5 )
    gfx.drawText( "xoffset: " .. tostring( offset ), 220, 5 )
    playdate.drawFPS( 380, 5 )
  end,
  
  Cleanup = function( self )
  end,
  
  ---------------------------------------------------------------------- HANDLE CRANK
  Handle_Crank = function( self )    
    local crankAngleDegrees = playdate.getCrankPosition()
    local crankChangeDegrees = playdate.getCrankChange()
    
    local crankRatio = crankAngleDegrees / 360
    
    state_test2["view"]["xOffset"] = -state_test2["backgroundWidth"] * crankRatio
    
    state_test2["view"]["viewXMin"] = -state_test2["view"]["xOffset"]
    state_test2["view"]["viewXMax"] = state_test2["view"]["viewXMin"] + 400
  end, -- Handle_Crank = function( self )
  
  
  -- Button handlers
  Handle_leftButtonDown = function( self )
  end,
  
  Handle_rightButtonDown = function( self )
  end,
  
  Handle_AButtonDown = function( self )
  end,
  
  Handle_BButtonDown      = function( self ) 
  end,
  
  
  -- Unused
  Handle_upButtonUp       = function( self ) 
  end,
  
  Handle_downButtonUp     = function( self ) 
  end,
  
  Handle_downButtonDown   = function( self ) 
  end,
  
  Handle_leftButtonUp     = function( self ) 
  end,
  
  Handle_upButtonDown  = function( self ) 
  end,
  
  Handle_rightButtonUp    = function( self ) 
  end,
  
  Handle_AButtonUp        = function( self ) 
  end,
  
  Handle_BButtonUp        = function( self ) 
  end,
}
