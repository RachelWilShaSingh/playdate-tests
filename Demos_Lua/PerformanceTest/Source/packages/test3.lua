import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'
local gfx = playdate.graphics

state_test3 = {
  nextState = "",   -- Set this when you're ready to go to another state
  
  keypresses = { up = false, down = false, left = false, right = false, a = false, b = false },
    
  images = {
    background = gfx.image.new( "images/test2/background.png" ),
    face = gfx.image.new( "images/test1/face.png" ),
  },
  
  tables = {
    fist = nil
  },
  
  backgroundWidth = 1600,
  
  view = {
    turnSpeed = 1,
    xOffset = 0,
    viewXMin = 0,
    viewXMax = 0,
    spawnMinX = 400,
    spawnMaxX = 1600-400
  },
  
  player = {
    score = 0,
    health = 100,
    healthMax = 100,
    punchCooldown = 0,
    punchCooldownMax = 25,
    punchDamage = 5
  },
  
  Init = function( self )
    state_test3.nextState = ""
    
    state_test3.tables["fist"], result = gfx.imagetable.new( "images/test2/punch" )
    if ( result ~= nil ) then print( "ERROR:", result ) end
  end,
  
  Update = function( self )
    state_test3.Handle_Crank()
    
    if ( state_test3["player"]["punchCooldown"] > 0 ) then
      state_test3["player"]["punchCooldown"] -= 1
    end
    
    if ( state_test3["keypresses"]["up"] ) then
      state_test3.BeginPunch()
    end
  end,
  
  Draw = function( self )
    local offset = state_test3["view"]["xOffset"]
    local bgWidth = state_test3["backgroundWidth"]
    
    gfx.clear( gfx.kColorWhite )
    gfx.setColor( gfx.kColorWhite )

    state_test3["images"]["background"]:draw( offset, 0 )
    state_test3["images"]["background"]:draw( offset + bgWidth, 0 )
    state_test3["images"]["face"]:draw( 100, 100 )
    
    if ( state_test3["player"]["punchCooldown"] > 0 ) then
      if     ( state_test3["player"]["punchCooldown"] < state_test3["player"]["punchCooldownMax"] * 0.25 ) then
        state_test3.tables["fist"]:getImage( 1 ):draw( 278, 75 )
      elseif ( state_test3["player"]["punchCooldown"] < state_test3["player"]["punchCooldownMax"] * 0.50 ) then
        state_test3.tables["fist"]:getImage( 2 ):draw( 278, 75 )
      elseif ( state_test3["player"]["punchCooldown"] < state_test3["player"]["punchCooldownMax"] * 0.75 ) then
        state_test3.tables["fist"]:getImage( 3 ):draw( 278, 75 )
      elseif ( state_test3["player"]["punchCooldown"] < state_test3["player"]["punchCooldownMax"] * 1.00 ) then
        state_test3.tables["fist"]:getImage( 4 ):draw( 278, 75 )
      end
    end
    
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 20 )
    gfx.drawText( "Use crank to move", 5, 5 )
    gfx.drawText( "xoffset: " .. tostring( offset ), 220, 5 )
    playdate.drawFPS( 380, 5 )
  end,
  
  Cleanup = function( self )
  end,
  
  ---------------------------------------------------------------------- HANDLE CRANK
  Handle_Crank = function( self )    
    local crankAngleDegrees = playdate.getCrankPosition()
    local crankChangeDegrees = playdate.getCrankChange()
    
    local crankRatio = crankAngleDegrees / 360
    
    state_test3["view"]["xOffset"] = -state_test3["backgroundWidth"] * crankRatio
    
    state_test3["view"]["viewXMin"] = -state_test3["view"]["xOffset"]
    state_test3["view"]["viewXMax"] = state_test3["view"]["viewXMin"] + 400
  end, -- Handle_Crank = function( self )
  
  ---------------------------------------------------------------------- BEGIN PUNCH
  BeginPunch = function( self )
    print( "PUNCH" )
    if ( state_test3["player"]["punchCooldown"] > 0 ) then return end -- Can't punch again
    state_test3["player"]["punchCooldown"] = state_test3["player"]["punchCooldownMax"]
  end, 
  
  
  -- Button handlers - toggle keypress flags on and off
  Handle_upButtonDown     = function( self )    state_test3["keypresses"]["up"] = true        end,
  Handle_upButtonUp       = function( self )    state_test3["keypresses"]["up"] = false       end,
  
  Handle_downButtonDown   = function( self )    state_test3["keypresses"]["down"] = true      end,
  Handle_downButtonUp     = function( self )    state_test3["keypresses"]["down"] = false     end,
  
  Handle_leftButtonDown   = function( self )    state_test3["keypresses"]["left"] = true      end,
  Handle_leftButtonUp     = function( self )    state_test3["keypresses"]["left"] = false     end,
  
  Handle_rightButtonDown  = function( self )    state_test3["keypresses"]["right"] = true     end,
  Handle_rightButtonUp    = function( self )    state_test3["keypresses"]["right"] = false    end,
  
  Handle_BButtonDown      = function( self )    state_test3["keypresses"]["b"] = true         end,
  Handle_BButtonUp        = function( self )    state_test3["keypresses"]["b"] = false        end,
  
  Handle_AButtonDown      = function( self )    state_test3["keypresses"]["b"] = true         end,
  Handle_AButtonUp        = function( self )    state_test3["keypresses"]["b"] = false        end,
}
