import 'CoreLibs/graphics.lua'
import 'CoreLibs/sprites.lua'
import 'utilities.lua'
local gfx = playdate.graphics
local snd = playdate.sound

state_test5 = {
  nextState = "",   -- Set this when you're ready to go to another state
  
  phase = "play",
  
  images = {
    background = gfx.image.new( "images/test2/background.png" ),
    laser = gfx.image.new( "images/test2/laser.png" ),
    skeleton = gfx.image.new( "images/test2/skeleton.png" ),
    warning = gfx.image.new( "images/test2/warning.png" ),
    splat = gfx.image.new( "images/test2/splat.png" ),
    shadow  = gfx.image.new( "images/transparent.png" ),
    textbox = gfx.image.new( "images/textbox.png" ),
  },
  
  tables = {
    fist = nil
  },
    
  view = {
    turnSpeed = 1,
    xOffset = 0,
    viewXMin = 0,
    viewXMax = 0,
    spawnMinX = 400,
    spawnMaxX = 1600-400
  },
  
  timer = {
    secondCounter = 0,
    timer = nil,
  },
  
  player = {
    score = 0,
    health = 100,
    healthMax = 100,
    punchCooldown = 0,
    punchCooldownMax = 25,
    punchDamage = 5
  },
  
  effects = {
  },
  
  shootCooldown = 0,
  shootCooldownMax = 50,
  createSkeleCooldown = 0,
  createSkeleCooldownMax = 50,

  background = {},
  bullets = {},
  skeletons = {},
  
  backgroundWidth = 1600,
  
  keypresses = { up = false, down = false, left = false, right = false, a = false, b = false },
    
  ---------------------------------------------------------------------- INIT
  Init = function( self )
    state_test5.nextState = ""
    
    local bg1 = gfx.sprite.new( state_test5["images"]["background"] )
    bg1:setCenter( 0, 0 )
    bg1:add()
    local bg2 = gfx.sprite.new( state_test5["images"]["background"] )
    bg2:setCenter( 0, 0 )
    bg2:add()
    
    state_test5.tables["fist"], result = gfx.imagetable.new( "images/test2/punch" )
    if ( result ~= nil ) then print( "ERROR:", result ) end
    
    table.insert( state_test5["background"], bg1 )
    table.insert( state_test5["background"], bg2 )
    
    -- Set up second timer
    --state_test5["timer"]["timer"] = playdate.timer.new( 1000, state_test5.SecondTimer )
    
    state_test5.CreateSkeleton()
  end, -- Init = function( self )
  
  ---------------------------------------------------------------------- CLEAN UP
  Cleanup = function( self )
    -- Free space here as needed, stop music
  end, -- Cleanup = function( self )
  
  ---------------------------------------------------------------------- UPDATE
  Update = function( self )  
    state_test5.Handle_Crank()
    --playdate.timer.updateTimers()
    gfx.sprite.update() -- update all sprite objects
    
    if ( state_test5["player"]["health"] <= 0 and state_test5["phase"] == "play" ) then
      state_test5["phase"] = "gameover"
      state_test5.GameOver()
    end
    
    if ( state_test5["phase"] == "gameover" ) then
      if ( state_test5.AnyKeyPressed() ) then
        state_test5.nextState = "skele_titleScreen"
      end
    
    else
      if ( state_test5["keypresses"]["up"] ) then
        state_test5.BeginPunch()
      elseif ( state_test5["keypresses"]["down"] ) then
      end
      
      state_test5.UpdateSkeletons()
      
      -- Cooldown timers
      if ( state_test5["shootCooldown"] > 0 ) then
        state_test5["shootCooldown"] -= 1
      end
      
      if ( state_test5["createSkeleCooldown"] > 0 ) then
        state_test5["createSkeleCooldown"] -= 1
      end
      
      if ( state_test5["player"]["punchCooldown"] > 0 ) then
        state_test5["player"]["punchCooldown"] -= 1
      end
      
      -- Adjust difficulty
      --if ( state_test5["timer"]["secondCounter"] > 3 and state_test5["createSkeleCooldown"] <= 0 ) then
        --if ( state_test5["timer"]["secondCounter"] < 60 and state_test5["timer"]["secondCounter"] % 20 == 0 ) then
          --state_test5.CreateSkeleton()
        --elseif ( state_test5["timer"]["secondCounter"] < 120 and state_test5["timer"]["secondCounter"] % 15 == 0 ) then
          --state_test5.CreateSkeleton()
        --elseif ( state_test5["timer"]["secondCounter"] < 180 and state_test5["timer"]["secondCounter"] % 10 == 0 ) then
          --state_test5.CreateSkeleton()
        --end
        
        --state_test5["createSkeleCooldown"] = state_test5["createSkeleCooldownMax"]
      --end
    end
    
  end, -- Update = function( self )
  
  ---------------------------------------------------------------------- CREATE SKELETON
  CreateSkeleton = function( self )
    local view = state_test5["view"]
    local newSkelly = {}
    -- skeleton must be spawned somewhere you're not looking...
    newSkelly["x"] = math.random( view["spawnMinX"], view["spawnMaxX" ] )
    newSkelly["y"] = 150
    newSkelly["z"] = 50
    newSkelly["width"] = 109
    newSkelly["height"] = 182
    newSkelly["sprite"] = gfx.sprite.new( state_test5["images"]["skeleton"] )
    newSkelly["sprite"]:setCenter( 0.5, 0.5 )
    newSkelly["sprite"]:moveTo( newSkelly["x"], newSkelly["y"] )
    newSkelly["sprite"]:add()
    newSkelly["counter"] = 0
    newSkelly["counterMax"] = 80
    newSkelly["speed"] = 0.5
    newSkelly["attackCounter"] = 0
    newSkelly["attackCounterMax"] = 100
    newSkelly["damage"] = 2
    newSkelly["hp"] = state_test5["timer"]["secondCounter"] + 3
    newSkelly["hpMax"] = state_test5["timer"]["secondCounter"] + 3
    newSkelly["damageCooldown"] = 0
    newSkelly["damageCooldownMax"] = 20
    newSkelly["dieCounter"] = 0
    newSkelly["dieCounterMax"] = 50
    table.insert( state_test5["skeletons"], newSkelly )
  end,
  
  ---------------------------------------------------------------------- UPDATE SKELETONS
  UpdateSkeletons = function( self )
    if ( state_test5["phase"] == "gameover" ) then return end
  
    local player = state_test5["player"]
    local view = state_test5["view"]
    
    deleteSkeletons = {}
    for key, skelly in pairs( state_test5["skeletons"] ) do
    
      -- Skeleton dead counter
      if ( skelly["dieCounter"] > 0 ) then
        skelly["sprite"]:setRotation( 90 )
        skelly["dieCounter"] -= 1
        if ( skelly["dieCounter"] <= 0 ) then
          table.insert( deleteSkeletons, key )
        end
        
      -- Skeleton is not dead
      else
      
        local distanceRatio = ( 200 - skelly["z"] ) / 200
        
        if ( skelly["counter"] < skelly["counterMax"] * 0.25 ) then
          skelly["sprite"]:setRotation( 35 )
        elseif ( skelly["counter"] < skelly["counterMax"] * 0.50 ) then
          skelly["sprite"]:setRotation( 0 )
        elseif ( skelly["counter"] < skelly["counterMax"] * 0.75 ) then
          skelly["sprite"]:setRotation( -35 )
        else
          skelly["sprite"]:setRotation( 0 )
        end
        
        
        -- Cooldowns      
        if ( skelly["damageCooldown"] > 0 ) then
          skelly["damageCooldown"] -= 1
        end
        
        -- Move toward player
        if ( skelly["z"] > 25 ) then
          skelly["z"] -= skelly["speed"]
          skelly["y"] += skelly["speed"] * 0.50
        end
      
        skelly["counter"] += 1
        if ( skelly["counter"] >= skelly["counterMax"] ) then
          skelly["counter"] = 0
        end
        
        if ( skelly["attackCounter"] > 0 ) then
          skelly["attackCounter"] -= 1
        end
        
        -- Close to player
        if ( skelly["z"] <= 25 ) then
          if ( skelly["attackCounter"] <= 0 ) then
            skelly["attackCounter"] = skelly["attackCounterMax"]
            state_test5.GetHurt( self, skelly["damage"] )
          end
        end
        
        -- Check for being punched
        if ( player["punchCooldown"] > 0 and skelly["z"] <= 50 ) then
          -- Is the player facing the skeleton?
          local skeleScreenX = skelly["x"] + state_test5["view"]["xOffset"]
          if ( skeleScreenX >= 200 and skeleScreenX <= 400 ) then
            -- Hit
            if ( skelly["damageCooldown"] <= 0 ) then
              skelly["hp"] -= player["punchDamage"]
              skelly["y"] -= 25
              skelly["z"] += 50
              skelly["damageCooldown"] = skelly["damageCooldownMax"]
            end
            
            if ( skelly["hp"] <= 0 ) then
              --table.insert( deleteSkeletons, key )
              skelly["dieCounter"] = skelly["dieCounterMax"] 
              player["score"] += 10
            end
          end
        end
      end
      end
      
    
    -- Delete destroyed skeletons
    for k, deleteKey in pairs( deleteSkeletons ) do
      state_test5["skeletons"][deleteKey]["sprite"]:remove()
      state_test5["skeletons"][deleteKey] = nil
    end
  end,
  
  ---------------------------------------------------------------------- BEGIN PUNCH
  BeginPunch = function( self )
    if ( state_test5["player"]["punchCooldown"] > 0 ) then return end -- Can't punch again
    state_test5["player"]["punchCooldown"] = state_test5["player"]["punchCooldownMax"]
  end, 
  
  ---------------------------------------------------------------------- GET HURT
  GetHurt = function( self, damage )
    local hurtEffect = {
      type = "hurtflash",
      timer = 0,
      timerMax = 50
    }
    table.insert( state_test5["effects"], hurtEffect )
    
    state_test5["player"]["health"] -= damage
  end,
  
  ---------------------------------------------------------------------- GAME OVER
  GameOver = function( self )  
    local timerCounter = state_test5["timer"]["secondCounter"]
    local score = state_test5["player"]["score"]
    -- Check for high score    
    if ( gamedata["highscores"] ~= nil and gamedata["highscores"]["skele_time"] ~= nil ) then      
      if ( timerCounter >= gamedata["highscores"]["skele_time"] ) then
        gamedata["highscores"]["skele_time"] = timerCounter
        SaveData()
      end
      
    else      
      if ( gamedata == nil ) then gamedata = {} end
      if ( gamedata["highscores"] == nil ) then gamedata["highscores"] = {} end
      
      gamedata["highscores"]["skele_time"] = timerCounter
      SaveData()    
    end
    
    if ( gamedata["highscores"] ~= nil and gamedata["highscores"]["skele_score"] ~= nil ) then      
      if ( score >= gamedata["highscores"]["skele_score"] ) then
        gamedata["highscores"]["skele_score"] = score
        SaveData()
      end
    else   
      if ( gamedata == nil ) then gamedata = {} end      
      if ( gamedata["highscores"] == nil ) then gamedata["highscores"] = {} end
      
      gamedata["highscores"]["skele_time"] = score
      SaveData()
    end
    
  end,
  
  ---------------------------------------------------------------------- DRAW
  Draw = function( self )
    local offset = state_test5["view"]["xOffset"]
    local bgWidth = state_test5["backgroundWidth"]
    
    state_test5["background"][1]:moveTo( offset, 0 )
    state_test5["background"][2]:moveTo( offset + bgWidth, 0 )
    
    if ( state_test5["phase"] == "gameover" ) then
      state_test5.images["shadow"]:draw( 0, 0 )
      state_test5.images["textbox"]:draw( 50, 30 )
      
      gfx.drawTextAligned( "GAME OVER", 400/2, 50, kTextAlignment.center )
      
      gfx.drawTextAligned( "You survived for " .. tostring( state_test5["timer"]["secondCounter"] ) .. " seconds.", 400/2, 85, kTextAlignment.center )
      gfx.drawTextAligned( "Your score was " .. tostring( state_test5["player"]["score"] ), 400/2, 105, kTextAlignment.center )

      if ( gamedata["highscores"] ~= nil and gamedata["highscores"]["skele_time"] ~= nil ) then
        gfx.drawTextAligned( "TOP TIME: " .. tostring( gamedata["highscores"]["skele_time"] ), 400/2, 135 , kTextAlignment.center )
        gfx.drawTextAligned( "HIGH SCORE: " .. tostring( gamedata["highscores"]["skele_score"] ), 400/2, 155 , kTextAlignment.center )
      end
      
    else
      for key, skelly in pairs( state_test5["skeletons"] ) do
        skelly["sprite"]:moveTo( skelly["x"] + offset, skelly["y"] )
        
        -- Adjust scale based on Z coordinate. 0 is close, 200 is far
        local scale = ( 200 - skelly["z"] ) / 100
        skelly["sprite"]:setScale( scale, scale )
        
        -- Draw health bar
        if ( skelly["dieCounter"] <= 0 ) then
          local healthRatio = skelly["hp"] / skelly["hpMax"]
          
          local barX = skelly["x"] + offset - (skelly["width"]/2)
          local barY = skelly["y"] - 110 * scale
          local barWidth = skelly["width"]
          gfx.setColor( gfx.kColorBlack )
          gfx.fillRect( barX, barY, barWidth, 10 )
          
          gfx.setColor( gfx.kColorWhite )
          gfx.fillRect( barX + 1, barY + 1, ( barWidth-1 ) * healthRatio, 8 )
        end
      end
      
      for key, laser in pairs( state_test5["bullets"] ) do
        -- Adjust laser position based on screen offset
        laser["sprite"]:moveTo( laser["x"] + offset, laser["y"] )
        
        local scale = ( 200 - laser["z"] ) / 100
        laser["sprite"]:setScale( scale, scale )
        
        gfx.setColor( gfx.kColorWhite )
        gfx.fillRect( 0, 220, 400, 20 )
        gfx.drawText( tostring( laser["sprite"].x ), 0, 220 )
        gfx.drawText( tostring( laser["sprite"].y ), 100, 220 )
      end
      
      -- Punch if punching
      if ( state_test5["player"]["punchCooldown"] > 0 ) then
        if     ( state_test5["player"]["punchCooldown"] < state_test5["player"]["punchCooldownMax"] * 0.25 ) then
          state_test5.tables["fist"]:getImage( 1 ):draw( 278, 75 )
        elseif ( state_test5["player"]["punchCooldown"] < state_test5["player"]["punchCooldownMax"] * 0.50 ) then
          state_test5.tables["fist"]:getImage( 2 ):draw( 278, 75 )
        elseif ( state_test5["player"]["punchCooldown"] < state_test5["player"]["punchCooldownMax"] * 0.75 ) then
          state_test5.tables["fist"]:getImage( 3 ):draw( 278, 75 )
        elseif ( state_test5["player"]["punchCooldown"] < state_test5["player"]["punchCooldownMax"] * 1.00 ) then
          state_test5.tables["fist"]:getImage( 4 ):draw( 278, 75 )
        end
      end
      
      local removeKeys = {}
      for key, effect in pairs( state_test5["effects"] ) do
        if ( effect["type"] == "hurtflash" ) then
          state_test5["images"]["splat"]:draw( 0, 0 )
          --state_test5["images"]["splat"]:drawBlurred( 0, 0, effect["timer"], 1, playdate.graphics.image.kDitherTypeScreen )
        end
        
        effect["timer"] += 1
        if ( effect["timer"] >= effect["timerMax"] ) then
          table.insert( removeKeys, key )
        end
      end
      
      -- Remove effects that have counters of 0
      for k, key in pairs( removeKeys ) do
        state_test5["effects"][key] = nil
      end
    end
    
    
    -- HUD
    local player = state_test5["player"]
    gfx.setColor( gfx.kColorWhite )
    gfx.fillRect( 0, 0, 400, 20 )
    gfx.drawTextAligned( tostring( player["health"] ) .. "/" .. tostring( player["healthMax"] ), 5, 0, kTextAlignment.left )
    gfx.drawTextAligned( tostring( state_test5["timer"]["secondCounter"] ), 200, 0, kTextAlignment.center )
    gfx.drawTextAligned( tostring( player["score"] ), 300, 0, kTextAlignment.right )
    playdate.drawFPS( 380, 5 )
  end, -- Draw = function( self )
  
  ---------------------------------------------------------------------- HANDLE CRANK
  Handle_Crank = function( self )
    if ( state_test5["phase"] == "gameover" ) then return end
    
    local crankAngleDegrees = playdate.getCrankPosition()
    local crankChangeDegrees = playdate.getCrankChange()
    
    local crankRatio = crankAngleDegrees / 360
    
    state_test5["view"]["xOffset"] = -state_test5["backgroundWidth"] * crankRatio
    
    state_test5["view"]["viewXMin"] = -state_test5["view"]["xOffset"]
    state_test5["view"]["viewXMax"] = state_test5["view"]["viewXMin"] + 400
  end, -- Handle_Crank = function( self )
  
  ---------------------------------------------------------------------- SECOND TIMER
  SecondTimer = function( self )
    if ( state_test5["phase"] == "gameover" ) then return end
    state_test5["timer"]["secondCounter"] += 1
    state_test5["timer"]["timer"] = playdate.timer.new( 1000, state_test5.SecondTimer )
  end,
  
  ---------------------------------------------------------------------- ANY KEY?
  AnyKeyPressed = function( self )
    return (
      state_test5["keypresses"]["up"] == true or
      state_test5["keypresses"]["down"] == true or
      state_test5["keypresses"]["left"] == true or
      state_test5["keypresses"]["right"] == true or
      state_test5["keypresses"]["a"] == true or
      state_test5["keypresses"]["b"] == true )
  end,
  
  ---------------------------------------------------------------------- BUTTON CALLBACKS
  -- Button handlers - toggle keypress flags on and off
  Handle_upButtonDown     = function( self )    state_test5["keypresses"]["up"] = true        end,
  Handle_upButtonUp       = function( self )    state_test5["keypresses"]["up"] = false       end,
  
  Handle_downButtonDown   = function( self )    state_test5["keypresses"]["down"] = true      end,
  Handle_downButtonUp     = function( self )    state_test5["keypresses"]["down"] = false     end,
  
  Handle_leftButtonDown   = function( self )    state_test5["keypresses"]["left"] = true      end,
  Handle_leftButtonUp     = function( self )    state_test5["keypresses"]["left"] = false     end,
  
  Handle_rightButtonDown  = function( self )    state_test5["keypresses"]["right"] = true     end,
  Handle_rightButtonUp    = function( self )    state_test5["keypresses"]["right"] = false    end,
  
  Handle_BButtonDown      = function( self )    state_test5["keypresses"]["b"] = true         end,
  Handle_BButtonUp        = function( self )    state_test5["keypresses"]["b"] = false        end,
  
  Handle_AButtonDown      = function( self )    state_test5["keypresses"]["b"] = true         end,
  Handle_AButtonUp        = function( self )    state_test5["keypresses"]["b"] = false        end,
}
