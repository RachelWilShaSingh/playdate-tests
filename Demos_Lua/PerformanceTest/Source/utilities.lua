function DoesTableContainElement( theTable, theElement )
  for key, element in ipairs( theTable ) do    
    if ( element == theElement ) then
      return true
    end
  end
  return false
end

function DoesTableContainKey( theTable, theKey )
  return ( theTable[theKey] ~= nil )
end

function GetDistance( obj1, obj2 )
  local xDiff = obj1["x"] - obj2["x"]
  local yDiff = obj1["y"] - obj2["y"]
  local dist = math.sqrt( xDiff * xDiff + yDiff * yDiff )
  return dist
end

function GetDistance3D( obj1, obj2 )
  local xDiff = obj1["x"] - obj2["x"]
  local yDiff = obj1["y"] - obj2["y"]
  local zDiff = obj1["z"] - obj2["z"]
  local dist = math.sqrt( xDiff * xDiff + yDiff * yDiff + zDiff * zDiff )
  return dist
end

function DebugTable( label, tab )
  print( "DEBUG \"" .. tostring( label ) .. "\"" )
  for key, value in pairs( tab ) do
    print( key, "=", value )
  end
end
