import 'collision.lua'

local gfx = playdate.graphics

function CreateBricks()
  print( "CreateBricks() in brick.lua" )
  
  bricks  = {}
  brickWidth = 50
  brickHeight = 25
  
  totalBricksWide = maxX / brickWidth
  
  index = 1
  for row = 0, 3, 1 do
    for col = 0, totalBricksWide, 1 do
      bricks[index] = {
        x = col * brickWidth,
        y = row * brickHeight,
        width = brickWidth,
        height = brickHeight,
        
        draw = function( self )
          gfx.setColor( gfx.kColorBlack )
          gfx.fillRect( self["x"], self["y"], self["width"], self["height"] ) -- playdate.graphics.fillRect(x, y, width, height)
          
          gfx.setColor( gfx.kColorWhite )
          gfx.drawRect( self["x"], self["y"], self["width"], self["height"] ) -- playdate.graphics.drawRect(x, y, w, h) 
        end
      }

      index = index + 1
    end
  end
  
  return bricks
end

function UpdateBricks( bricks, ball )
  removeKeys = {}
  removeKeysCount = 0
  
  collision = false

  for key, brick in pairs( bricks ) do
    if ( BoundingBoxCollision( brick, ball ) ) then
      -- Add to list of items to remove
      table.insert( removeKeys, key ) 
      removeKeysCount += 1      
      collision = true
    end
  end

  -- Remove brick(s) to be removed...
  for i = removeKeysCount, 1, -1 do
    bricks[ removeKeys[i] ] = nil
  end
  
  if ( collision == true ) then
    --ball:deaccelerate()
    ball:bounceX()
    ball:bounceY()
  end
end
