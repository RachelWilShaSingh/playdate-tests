
local gfx = playdate.graphics

function CreatePaddle()
  print( "CreatePaddles() in paddle.lua" )
  
  paddle = {
    x = maxX / 2,
    y = maxY - 15,
    width = 40,
    height = 30,
    
    draw = function( self )
      gfx.fillRect( self["x"], self["y"], self["width"], self["height"] ) -- playdate.graphics.fillRect(x, y, width, height)
      
      a  = paddle["x"] --+ ( 0 * paddle["width"] / 3 )
      b  = paddle["x"] + ( 1 * paddle["width"] / 3 )
      c  = paddle["x"] + ( 2 * paddle["width"] / 3 )
      d  = paddle["x"] + ( 3 * paddle["width"] / 3 )
      
      gfx.drawLine( b, paddle["y"], b, paddle["y"] + paddle["height"] )
      gfx.drawLine( c, paddle["y"], c, paddle["y"] + paddle["height"] )
    end,
    
    update = function( self )
      crankDegrees = playdate.getCrankPosition() -- 0 = straight up, 359.999 = straight down
      crankChange, crankAcc = playdate.getCrankChange()
      
      self["x"] += crankChange
      if ( self["x"] < minX ) then 
        self["x"] = minX
      elseif ( self["x"] + self["width"] > maxX ) then
        self["x"] = maxX - self["width"]
      end
    end
  }
  
  return paddle
end
