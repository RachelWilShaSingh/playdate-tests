
function BoundingBoxCollision( objA, objB )
  return (    objA["x"]                 <=  objB["x"] + objB["width"] and   -- LEFT1 <= RIGHT2
              objA["x"] + objA["width"] >=  objB["x"] and                   -- RIGHT1 >= LEFT2
              objA["y"]                 <=  objB["y"] + objB["height"] and  -- TOP1 <= BOTTOM2
              objA["y"] + objA["height"] >= objB["y"] )                     -- BOTTOM1 >= TOP2
end

--function PaddleCollision( ball, padd )
  --return (    ball["x"]                 <=  padd["x"] + padd["width"] and   -- LEFT1 <= RIGHT2
              --ball["x"] + ball["width"] >=  padd["x"] and                   -- RIGHT1 >= LEFT2
              --ball["y"]                 <=  padd["y"] + padd["height"] and  -- TOP1 <= BOTTOM2
              --ball["y"] + ball["height"] >= padd["y"] )                     -- BOTTOM1 >= TOP2
--end
