
local gfx = playdate.graphics

function CreateBall()
  print( "CreateBall() in ball.lua" )
  
  cx = maxX / 2
  cy = maxY / 2
  r = 5
  
  ball = {
    centerX = cx,
    centerY = cy,
    radius = r,
    velX = 0,
    velY = 2,
    maxVel = 5,
    acc = 0.5,
    x = cx - r,
    y = cy - r,
    width = r * 2,
    height = r * 2,
    bounceTimeout = 0,
    
    draw = function( self )
      gfx.setColor( gfx.kColorXOR )
      gfx.fillCircleAtPoint( self["centerX"], self["centerY"], self["radius"] )
      gfx.fillCircleAtPoint( self["centerX"], self["centerY"], 2 )
      -- Bounding box:
      --gfx.drawRect( self["x"], self["y"], self["width"], self["height"] )
    end,
    
    accelerate = function( self )
      -- Add to velocity
      if ( self["velX"] < 0 ) then
        self["velX"] -= self["acc"]
      elseif ( self["velX"] > 0 ) then
        self["velX"] += self["acc"]
      end
      
      -- Don't go faster than max velocity
      if ( self["velX"] > self["maxVel"] ) then
        self["velX"] = self["maxVel"]
      elseif ( self["velX"] < -self["maxVel"] ) then
        self["velX"] = -self["maxVel"]
      end
      
      -- Add to velocity
      if ( self["velY"] < 0 ) then
        self["velY"] -= self["acc"]
      elseif ( self["velY"] > 0 ) then
        self["velY"] += self["acc"]
      end
      
      -- Don't go faster than max velocity
      if ( self["velY"] > self["maxVel"] ) then
        self["velY"] = self["maxVel"]
      elseif ( self["velY"] < -self["maxVel"] ) then
        self["velY"] = -self["maxVel"]
      end
    end,
    
    deaccelerate = function( self )
      if ( self["velX"] < 0 ) then
        self["velX"] += self["acc"]
      elseif ( self["velX"] > 0 ) then
        self["velX"] -= self["acc"]
      end
      
      if ( self["velY"] < 0 ) then
        self["velY"] += self["acc"]
      elseif ( self["velY"] > 0 ) then
        self["velY"] -= self["acc"]
      end
    end,
    
    bounceX = function( self )
      self["velX"] = -self["velX"]
    end,
    
    bounceY = function( self )
      self["velY"] = -self["velY"]
    end,
    
    leftVelocity = function( self )
      if      ( self["velX"] == 0 ) then
        self["velX"] = -2
      elseif  ( self["velX"] > 0 ) then
        self["velX"] = -self["velX"]
      end
    end,
    
    centerVelocity = function( self )
      self["velX"] = 0
    end,
    
    rightVelocity = function( self )
      if      ( self["velX"] == 0 ) then
        self["velX"] = 2
      elseif  ( self["velX"] < 0 ) then
        self["velX"] = -self["velX"]
      end
    end,
    
    upVelocity = function( self )
      if ( self["velY"] > 0 ) then
        self["velY"] = -self["velY"]
      end
    end,
    
    bounceOffPaddle = function( self, paddle )
      self:upVelocity()
      self:accelerate()
      
      a  = paddle["x"] --+ ( 0 * paddle["width"] / 3 )
      b  = paddle["x"] + ( 1 * paddle["width"] / 3 )
      c  = paddle["x"] + ( 2 * paddle["width"] / 3 )
      d  = paddle["x"] + ( 3 * paddle["width"] / 3 )
      
      if      ( self["centerX"] >= a and self["centerX"] <= b ) then -- left side of paddle
        self:leftVelocity()
      elseif  ( self["centerX"] >  b and self["centerX"] <  c ) then -- center side of paddle
        self:centerVelocity()
      elseif  ( self["centerX"] >= c and self["centerX"] <= d ) then -- right side of paddle
        self:rightVelocity()
      end
      
      self["centerY"] = paddle["y"] - 2
    end,
    
    reset = function( self )
      self["velX"] = 0
      self["velY"] = 2
      self["centerX"] = 400/2
      self["centerY"] = 240/2
    end,
    
    update = function( self, paddle )
      self["centerX"] += self["velX"]
      self["centerY"] += self["velY"]
      
      -- Hitting the paddle?
      if ( self["bounceTimeout"] <= 0 and BoundingBoxCollision( self, paddle ) ) then
        self:bounceOffPaddle( paddle )
        self["bounceTimeout"] = 50
      end
      
      if ( self["bounceTimeout"] > 0 ) then
        self["bounceTimeout"] -= 1
      end
      
      -- Hitting edge of the screen?
      if ( self["centerX"] > maxX or self["centerX"] < minX ) then
        self:bounceX()
      end
      
      if ( self["centerY"] < minY ) then
        self:bounceY()
      end
      
      -- Off the bottom
      if ( self["centerY"] > maxY ) then
        self:reset()
      end
      
      -- Update bounding box
      self["x"] = self["centerX"] - self["radius"]
      self["y"] = self["centerY"] - self["radius"]
    end
  }
  
  return ball
end
