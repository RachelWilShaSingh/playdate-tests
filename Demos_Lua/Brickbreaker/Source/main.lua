import 'CoreLibs/graphics.lua'
import 'CoreLibs/crank.lua'

import 'ball.lua'
import 'paddle.lua'
import 'brick.lua'
import 'collision.lua'

print( "Example ~ Brick breaker ~ RWSS" )

local gfx = playdate.graphics
playdate.display.setRefreshRate( 50 )

gfx.setBackgroundColor( gfx.kColorWhite )

minX = 0
maxX = 400
minY = 0
maxY = 240

paused = true

function playdate.update()  
  -- Pause before starting game
  if ( paused ) then
    -- nothing
  else
  -- ##################################### UPDATES #
  paddle:update()                         -- Update paddle
  ball:update( paddle )                   -- Update ball
  UpdateBricks( bricks, ball )            -- Update bricks
  end
  
  -- ##################################### DRAWING #
  gfx.clear( gfx.kColorWhite )            -- Clear screen

  for key, brick in pairs( bricks ) do    -- Draw bricks
    brick:draw()
  end
  
  ball:draw()                             -- Draw ball
  
  paddle:draw()                           -- Draw paddle
end

function playdate.BButtonDown()
  paused = false
end

-- Program begin:
bricks  = CreateBricks()
paddle  = CreatePaddle()
ball    = CreateBall()

print( "3:53" )
